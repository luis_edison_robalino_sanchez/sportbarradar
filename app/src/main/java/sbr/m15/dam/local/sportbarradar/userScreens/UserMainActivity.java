package sbr.m15.dam.local.sportbarradar.userScreens;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;

import com.ncapdevi.fragnav.FragNavController;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabReselectListener;
import com.roughike.bottombar.OnTabSelectListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import sbr.m15.dam.local.sportbarradar.R;
import sbr.m15.dam.local.sportbarradar.bases.BaseActivity;

public class UserMainActivity extends BaseActivity {

    @BindView(R.id.bottomBar)
    BottomBar bottomBar;

    private FragNavController fragNavController;

    private final int INDEX_PARTIDOS = FragNavController.TAB1;
    private final int INDEX_MAPA = FragNavController.TAB2;
    private final int INDEX_PERFIL = FragNavController.TAB3;

    private Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_main);
        activity = this;

        List<Fragment> fragments = new ArrayList<>(3);

        fragments.add(UserTabsPartidosFragment.newInstance());
        fragments.add(UserMapFragment.newInstance());
        fragments.add(UserProfileFragment.newInstance());
        fragNavController = new FragNavController(savedInstanceState, getSupportFragmentManager(), R.id.container, fragments, INDEX_MAPA);
        bottomBar.selectTabAtPosition(INDEX_MAPA);
        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {
                switch (tabId) {
                    case R.id.tab_equipos:
                        fragNavController.switchTab(INDEX_PARTIDOS);
                        break;
                    case R.id.tab_mapa:
                        fragNavController.switchTab(INDEX_MAPA);
                        break;
                    case R.id.tab_perfil:
                        fragNavController.switchTab(INDEX_PERFIL);
                        break;
                }

            }
        });
        bottomBar.setOnTabReselectListener(new OnTabReselectListener() {
            @Override
            public void onTabReSelected(@IdRes int tabId) {
                fragNavController.clearStack();
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        bottomBar.onSaveInstanceState();
        if (fragNavController != null) {
            fragNavController.onSaveInstanceState(outState);
        }
    }

    @Override
    public void onBackPressed() {
        boolean salir = true;
        Fragment f = getSupportFragmentManager().findFragmentByTag("mapa");
        if (f != null) {
            getSupportFragmentManager().beginTransaction().remove(f).commit();
            salir = false;
        } else {
            f = getSupportFragmentManager().findFragmentByTag("baresDeXPartido");
            if (f != null) {
                getSupportFragmentManager().beginTransaction().remove(f).commit();
                salir = false;
            } else {
                f = getSupportFragmentManager().findFragmentByTag("partidosDeXLiga");
                if (f != null) {
                    getSupportFragmentManager().beginTransaction().remove(f).commit();
                    salir = false;
                }
            }
        }

        if (salir) {
            if (fragNavController.getCurrentStack().size() > 1) {
                fragNavController.popFragment();
            } else {
                super.onBackPressed();
            }
        }
    }

}
