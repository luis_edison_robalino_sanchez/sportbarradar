package sbr.m15.dam.local.sportbarradar.homeScreens.presenter;

import sbr.m15.dam.local.sportbarradar.homeScreens.interactor.RegisterBarInteractorImpl;
import sbr.m15.dam.local.sportbarradar.homeScreens.interactor.interfaces.RegisterBarInteractor;
import sbr.m15.dam.local.sportbarradar.homeScreens.presenter.interfaces.RegisterBarPresenter;
import sbr.m15.dam.local.sportbarradar.homeScreens.view.interfaces.RegisterBarView;
import sbr.m15.dam.local.sportbarradar.model.Bar;

public class RegisterBarPresenterImpl implements RegisterBarPresenter, RegisterBarInteractor.OnRegisterBarFinishedListener {
    private RegisterBarView vista;
    private RegisterBarInteractor interactor;

    public RegisterBarPresenterImpl(RegisterBarView vista) {
        this.vista = vista;
        this.interactor = new RegisterBarInteractorImpl(this);
    }

    @Override
    public void validateRegister(Bar bar) {
        if (vista != null) {
            vista.cleanErrors();
            vista.showProgress();
        }
        interactor.validateRegister(bar);
    }

    @Override
    public void onDestroy() {
        vista = null;
    }

    @Override
    public void onErrorEmptyName() {
        vista.setErrorEmptyName();
    }

    @Override
    public void onErrorNotValidMail() {
        vista.setErrorNotValidMail();
    }

    @Override
    public void onErrorEmptyMail() {
        vista.setErrorEmptyMail();
    }

    @Override
    public void onErrorMail() {
        vista.setErrorMail();
    }

    @Override
    public void onErrorNotConection() {
        vista.setErrorNotConnection();
    }

    @Override
    public void onErrorNotSelectedPosition() {
        vista.setErrorEmptyPosition();
    }

    @Override
    public void onErrorNotValidPosition() {
        vista.setErrorNotValidPosition();
    }

    @Override
    public void onSuccess() {
        vista.redirect();
    }

    @Override
    public void sendPassword(String mail, String password) {
        vista.sendPassword(mail, password);
    }
}
