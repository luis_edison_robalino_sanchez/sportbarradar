package sbr.m15.dam.local.sportbarradar.barScreens;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import butterknife.BindView;
import sbr.m15.dam.local.sportbarradar.R;
import sbr.m15.dam.local.sportbarradar.adapter.MatchesLeagueLongAdapter;
import sbr.m15.dam.local.sportbarradar.bases.BaseFragment;
import sbr.m15.dam.local.sportbarradar.db.Connexion;
import sbr.m15.dam.local.sportbarradar.model.Match;

public class BarListMatchesFragment extends BaseFragment {

    @BindView(R.id.listadoPartidos)
    RecyclerView listadoPartidos;

    private MatchesLeagueLongAdapter adapter;
    private ArrayList<Match> matches;
    private View onClickView;

    public BarListMatchesFragment() {
    }

    public static BarListMatchesFragment newInstance() {
        BarListMatchesFragment fragment = new BarListMatchesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        showMatchesList();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_list_matches, container, false);
    }

    private void showMatchesList() {
        matches = new ArrayList<>();
        Connexion conn = new Connexion();
        try {
            conn.conectar();
            matches = conn.getMatchesFromBar(getUidFromFile());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conn.desconectar();
        }
        adapter = new MatchesLeagueLongAdapter(matches);
        adapter.setLongOnClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                onClickView = v;
                showConfirmationDeleteMatchDialog();
                return true;
            }
        });
        listadoPartidos.setAdapter(adapter);
        listadoPartidos.setLayoutManager(
                new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
    }

    private void showConfirmationDeleteMatchDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage(R.string.dialogDeleteMatchBar)
                .setPositiveButton(R.string.btnAccept, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        deleteMatch();
                    }
                })
                .setNegativeButton(R.string.btnCancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void deleteMatch() {
        int pos = listadoPartidos.getChildAdapterPosition(onClickView);
        Match match = matches.get(pos);
        Connexion conn = new Connexion();
        try {
            conn.conectar();
            conn.deleteMatchFromBar(match.getUidBar(), match.getApi());
            Snackbar.make(getView(), R.string.msgDeleteMatch, Snackbar.LENGTH_SHORT).show();
            matches.remove(pos);
            adapter.notifyItemRemoved(pos);
        } catch (Exception e) {
            Snackbar.make(getView(), R.string.errorNotConnection, Snackbar.LENGTH_SHORT).show();
            e.printStackTrace();
        } finally {
            if (conn.isConnected()) conn.desconectar();
        }
    }
}
