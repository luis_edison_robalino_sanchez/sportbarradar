package sbr.m15.dam.local.sportbarradar.homeScreens.presenter.interfaces;

import sbr.m15.dam.local.sportbarradar.model.User;

public interface RegisterUserPresenter {
    void validateRegister(User user);

    void onDestroy();
}
