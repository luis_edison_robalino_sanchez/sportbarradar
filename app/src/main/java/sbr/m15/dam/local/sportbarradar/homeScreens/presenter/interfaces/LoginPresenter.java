package sbr.m15.dam.local.sportbarradar.homeScreens.presenter.interfaces;

public interface LoginPresenter {
    void validateCredentials(String username, String password);

    void onDestroy();

    void recoveryAccept(String email);
}
