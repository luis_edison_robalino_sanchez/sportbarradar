package sbr.m15.dam.local.sportbarradar.dialogs;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseNetworkException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;

import butterknife.BindView;
import butterknife.OnClick;
import sbr.m15.dam.local.sportbarradar.R;
import sbr.m15.dam.local.sportbarradar.bases.BaseDialogFragment;
import sbr.m15.dam.local.sportbarradar.utils.FormUtils;

public class ChangePasswordDialog extends BaseDialogFragment {

    @BindView(R.id.dialogPasswordOld)
    TextInputLayout dialogPasswordOld;
    @BindView(R.id.dialogEditTextPasswordOld)
    TextInputEditText dialogEditTextPasswordOld;
    @BindView(R.id.dialogPasswordNew)
    TextInputLayout dialogPasswordNew;
    @BindView(R.id.dialogEditTextPasswordNew)
    TextInputEditText dialogEditTextPasswordNew;
    @BindView(R.id.dialogPasswordRepeat)
    TextInputLayout dialogPasswordRepeat;
    @BindView(R.id.dialogEditTextPasswordRepeat)
    TextInputEditText dialogEditTextPasswordRepeat;

    private String email;
    private FirebaseAuth mAuth;
    private View rootView;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mAuth = FirebaseAuth.getInstance();
        email = getArguments().getString("email");
        rootView = getActivity().getWindow().getDecorView().getRootView();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_change_password, container, false);
    }

    @OnClick({R.id.cancelChangePassword, R.id.sendChangePassword})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cancelChangePassword:
                dismiss();
                break;
            case R.id.sendChangePassword:
                if (formChangePasswordIsCorrect()) {
                    changePassword();
                }
                break;
        }
    }

    private boolean formChangePasswordIsCorrect() {
        Boolean isCorrect = true;

        String passwordOld = dialogEditTextPasswordOld.getText().toString();
        String passwordNew = dialogEditTextPasswordNew.getText().toString();
        String passwordRepeat = dialogEditTextPasswordRepeat.getText().toString();

        clearErrors();

        if (!FormUtils.isNotEmpty(passwordOld)) {
            dialogPasswordOld.setError(getString(R.string.errorEmptyField));
            isCorrect = false;
        }

        if (passwordNew.length() < 6) {
            dialogPasswordNew.setError(getString(R.string.errorShortPassword));
            isCorrect = false;
        }

        if (!FormUtils.isNotEmpty(passwordNew)) {
            dialogPasswordNew.setError(getString(R.string.errorEmptyField));
            isCorrect = false;
        }

        if (!FormUtils.isNotEmpty(passwordRepeat)) {
            dialogPasswordRepeat.setError(getString(R.string.errorEmptyField));
            isCorrect = false;
        }

        if (FormUtils.isNotEmpty(passwordNew) && FormUtils.isNotEmpty(passwordRepeat)
                && !passwordNew.equals(passwordRepeat)) {
            dialogPasswordNew.setError(getString(R.string.errorPasswordNotEqual));
            isCorrect = false;
        }

        return isCorrect;
    }

    private void clearErrors() {
        dialogPasswordOld.setError("");
        dialogPasswordNew.setError("");
        dialogPasswordRepeat.setError("");
    }

    private void changePassword() {
        mAuth.signInWithEmailAndPassword(email, dialogEditTextPasswordOld.getText().toString())
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                            user.updatePassword(dialogEditTextPasswordNew.getText().toString())
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                dismiss();
                                                Snackbar.make(rootView, R.string.msgChangedPassword, Snackbar.LENGTH_SHORT).show();
                                            } else {
                                                try {
                                                    throw task.getException();
                                                } catch (FirebaseNetworkException e) {
                                                    Snackbar.make(rootView, R.string.errorNotConnection, Snackbar.LENGTH_SHORT).show();
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                    Log.e("ERROR CHANGE PASSWORD:", e.getMessage());
                                                }
                                            }
                                        }
                                    });
                        } else {
                            try {
                                throw task.getException();
                            } catch (FirebaseAuthInvalidCredentialsException e) {
                                dialogPasswordOld.setError(getString(R.string.errorIncorrectPassword));
                            } catch (FirebaseNetworkException e) {
                                Snackbar.make(rootView, R.string.errorNotConnection, Snackbar.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                e.printStackTrace();
                                Log.e("ERROR CHANGE PASSWORD:", e.getMessage());
                            }
                        }
                    }
                });
    }
}
