package sbr.m15.dam.local.sportbarradar.viewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import sbr.m15.dam.local.sportbarradar.R;
import sbr.m15.dam.local.sportbarradar.model.Valoration;

public class ValorationsViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.nameBarUser)
    TextView nameBarUser;
    @BindView(R.id.descriptionValoration)
    TextView descriptionValoration;
    @BindView(R.id.ratingValoration)
    RatingBar ratingValoration;

    public ValorationsViewHolder(View itemView) {
        super(itemView);
        View rootView = itemView;
        ButterKnife.bind(this, rootView);
    }

    public void bindValoration(Valoration v) {
        nameBarUser.setText(v.getName());
        descriptionValoration.setText(v.getDescription());
        ratingValoration.setRating(v.getPuntuation());
    }
}
