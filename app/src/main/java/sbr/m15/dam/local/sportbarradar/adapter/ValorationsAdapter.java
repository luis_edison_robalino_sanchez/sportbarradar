package sbr.m15.dam.local.sportbarradar.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.Serializable;
import java.util.ArrayList;

import sbr.m15.dam.local.sportbarradar.R;
import sbr.m15.dam.local.sportbarradar.model.Valoration;
import sbr.m15.dam.local.sportbarradar.viewHolder.ValorationsViewHolder;

public class ValorationsAdapter extends RecyclerView.Adapter<ValorationsViewHolder> implements Serializable {
    ArrayList<Valoration> valorations;

    public ValorationsAdapter(ArrayList<Valoration> valorations) {
        this.valorations = valorations;
    }

    @Override
    public ValorationsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_valoration, parent, false);
        ValorationsViewHolder vvh = new ValorationsViewHolder(itemView);
        return vvh;
    }

    @Override
    public void onBindViewHolder(ValorationsViewHolder holder, int position) {
        holder.bindValoration(valorations.get(position));
    }

    public void refreshData(ArrayList<Valoration> valorations) {
        this.valorations.clear();
        this.valorations.addAll(valorations);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return valorations.size();
    }

}
