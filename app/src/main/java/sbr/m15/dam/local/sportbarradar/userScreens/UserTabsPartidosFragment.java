package sbr.m15.dam.local.sportbarradar.userScreens;

import android.os.Bundle;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import sbr.m15.dam.local.sportbarradar.R;
import sbr.m15.dam.local.sportbarradar.bases.BaseFragment;

public class UserTabsPartidosFragment extends BaseFragment {
    private FragmentTabHost mTabHost;

    public UserTabsPartidosFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_tabs_partidos, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mTabHost = (FragmentTabHost) view.findViewById(R.id.tabs);
        mTabHost.setup(getActivity(), getChildFragmentManager(), android.R.id.tabcontent);

        mTabHost.addTab(mTabHost.newTabSpec("byTeam").setIndicator(getString(R.string.topBarFavTeam)),
                UserListFavTeamFragment.class, null);
        mTabHost.addTab(mTabHost.newTabSpec("byBar").setIndicator(getString(R.string.topBarFavBar)),
                UserListFavBarFragment.class, null);
        mTabHost.addTab(mTabHost.newTabSpec("byMatches").setIndicator(getString(R.string.topBarMatches)),
                UserListLeaguesFragment.class, null);

        for (int i = 0; i < mTabHost.getTabWidget().getTabCount(); i++)
            mTabHost.getTabWidget().getChildAt(i).getLayoutParams().height =
                    (int) (50 * this.getResources().getDisplayMetrics().density);
    }

    public static UserTabsPartidosFragment newInstance() {
        UserTabsPartidosFragment fragment = new UserTabsPartidosFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mTabHost != null)
            mTabHost = null;
    }
}
