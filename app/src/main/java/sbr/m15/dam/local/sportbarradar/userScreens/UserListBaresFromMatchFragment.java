package sbr.m15.dam.local.sportbarradar.userScreens;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.ArrayList;

import butterknife.BindView;
import sbr.m15.dam.local.sportbarradar.R;
import sbr.m15.dam.local.sportbarradar.adapter.BaresAdapter;
import sbr.m15.dam.local.sportbarradar.bases.BaseFragment;
import sbr.m15.dam.local.sportbarradar.db.Connexion;
import sbr.m15.dam.local.sportbarradar.model.Bar;
import sbr.m15.dam.local.sportbarradar.model.Match;

public class UserListBaresFromMatchFragment extends BaseFragment implements LocationListener, BaresAdapter.BarItemClickListener {

    @BindView(R.id.listadoLigas)
    RecyclerView listadoLigas;
    @BindView(R.id.pbHeaderProgress)
    ProgressBar pbHeaderProgress;

    private ArrayList<Bar> bares;
    private BaresAdapter bAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    LocationManager locationManager;
    LocationListener locationListener;
    Location location;

    public UserListBaresFromMatchFragment() {
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pbHeaderProgress.setVisibility(View.VISIBLE);
        instanceLocation();
        bares = new ArrayList<Bar>();
        Connexion conn = new Connexion();
        try {
            conn.conectar();
            if (location != null) {
                bares = conn.getBaresFromMatch(getArguments().getInt("partido"), location.getAltitude(), location.getLongitude());
            } else {
                bares = conn.getBaresFromMatch(getArguments().getInt("partido"));
            }
        } catch (Exception e) {
            Snackbar.make(getView(), R.string.errorNotConnection, Snackbar.LENGTH_SHORT).show();
            e.printStackTrace();
        } finally {
            if (conn.isConnected()) conn.desconectar();
        }
        bAdapter = new BaresAdapter(bares, getContext(), this);
        mLayoutManager = new LinearLayoutManager(getContext());
        listadoLigas.setLayoutManager(mLayoutManager);
        listadoLigas.setAdapter(bAdapter);
        pbHeaderProgress.setVisibility(View.GONE);
    }

    private void instanceLocation() {
        locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
        locationListener = new UserListBaresFromMatchFragment();
        //noinspection MissingPermission
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME, MIN_DISTANCE, locationListener);
        //noinspection MissingPermission
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME, MIN_DISTANCE, locationListener);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list_leagues, container, false);
    }

    public static UserListBaresFromMatchFragment newInstance(Match m) {
        UserListBaresFromMatchFragment fragment = new UserListBaresFromMatchFragment();
        Bundle args = new Bundle();
        args.putInt("partido", m.getApi());
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onLocationChanged(Location location) {
        this.location = location;
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void itemClicked(Bar b) {
        showDialogConfirmRedirectToMap(b);
    }
}
