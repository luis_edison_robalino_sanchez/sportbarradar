package sbr.m15.dam.local.sportbarradar.bases;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.siyamed.shapeimageview.mask.PorterShapeImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import sbr.m15.dam.local.sportbarradar.R;
import sbr.m15.dam.local.sportbarradar.db.Connexion;
import sbr.m15.dam.local.sportbarradar.dialogs.ChangePasswordDialog;
import sbr.m15.dam.local.sportbarradar.model.Bar;
import sbr.m15.dam.local.sportbarradar.model.SavedLogin;
import sbr.m15.dam.local.sportbarradar.model.User;

public class ProfileBaseFragment extends BaseFragment {

    protected static final int RESULT_OK = -1;
    protected static final int CAMERA_REQUEST = 2;
    protected static final int GALLERY_REQUEST = 3;

    private static final int CAMERA_MULTIPLE_PERMISSIONS = 1;
    private static final int GALLERY_MULTIPLE_PERMISSIONS = 2;

    private final String APP_DIRECTORY = Environment.getExternalStorageDirectory().toString() + "/RadarSportBar";
    private final String PHOTO_CAMERA = APP_DIRECTORY + "/profilePhoto.jpg";

    @BindView(R.id.imgProfile)
    PorterShapeImageView imgProfile;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setImageProfile();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    private void setImageProfile() {
        byte[] foto = (getSavedLoginFromFile().getRole().equals("user")) ?
                getUserFromFile().getFoto() : getBarFromFile().getFoto();
        if (foto != null) {
            imgProfile.setImageBitmap(BitmapFactory.decodeByteArray(foto, 0, foto.length));
        }
    }

    protected void logout() {
        deleteFileSavedLogin();
        deleteFileUserData();
        deleteFileBarData();
        getActivity().finish();
    }

    protected void changePasswordDialog() {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        ChangePasswordDialog newFragment = new ChangePasswordDialog();
        Bundle args = new Bundle();
        args.putString("email", getEmailFromFile());
        newFragment.setArguments(args);
        newFragment.setCancelable(false);
        newFragment.show(fragmentManager, "dialog");
    }

    protected void changePhotoProfileDialog() {
        final CharSequence[] options = {getString(R.string.btnTakePhoto),
                getString(R.string.btnGallery), getString(R.string.btnCancel)};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.titlePhotoProfileDialog)
                .setItems(options, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        if (options[item].equals(getString(R.string.btnTakePhoto))) {
                            requestCamera();
                        } else if (options[item].equals(getString(R.string.btnGallery))) {
                            requestGallery();
                        } else if (options[item].equals(getString(R.string.btnCancel))) {
                            dialog.dismiss();
                        }
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private ArrayList<String> getCameraPermissions() {
        String[] permissions = {
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
        };
        ArrayList<String> listPermissions = new ArrayList<>();
        for (String permission : permissions)
            if (ContextCompat.checkSelfPermission(getContext(), permission) == PackageManager.PERMISSION_DENIED)
                listPermissions.add(permission);

        return listPermissions;
    }

    private void requestCamera() {
        requestPermissionsAndAction(getCameraPermissions(), CAMERA_MULTIPLE_PERMISSIONS);
        if (!getCameraPermissions().isEmpty())
            showRequestPermissionsDialog(CAMERA_MULTIPLE_PERMISSIONS);
        else camera();
    }

    private void camera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        new File(APP_DIRECTORY).mkdirs();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            Uri contentUri = FileProvider.getUriForFile(getContext(), "sbr.m15.dam.local.sportbarradar.fileProvider", new File(PHOTO_CAMERA));
            intent.putExtra(MediaStore.EXTRA_OUTPUT, contentUri);
        } else {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(PHOTO_CAMERA)));
        }
        startActivityForResult(intent, CAMERA_REQUEST);
    }

    private ArrayList getGalleryPermissions() {
        String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE};
        ArrayList<String> listPermissions = new ArrayList<>();
        for (String permission : permissions)
            if (ContextCompat.checkSelfPermission(getContext(), permission) == PackageManager.PERMISSION_DENIED)
                listPermissions.add(permission);

        return listPermissions;
    }

    private void requestGallery() {
        requestPermissionsAndAction(getGalleryPermissions(), GALLERY_MULTIPLE_PERMISSIONS);
        if (!getGalleryPermissions().isEmpty())
            showRequestPermissionsDialog(GALLERY_MULTIPLE_PERMISSIONS);
        else gallery();
    }

    private void gallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, GALLERY_REQUEST);
    }

    private void requestPermissionsAndAction(ArrayList<String> listPermissions, int id) {
        if (!listPermissions.isEmpty()) {
            requestPermissions(listPermissions.toArray(new String[listPermissions.size()]), id);
        } else {
            if (id == CAMERA_MULTIPLE_PERMISSIONS) camera();
            else if (id == GALLERY_MULTIPLE_PERMISSIONS) gallery();
        }
    }

    private void updatePhotoProfile(String path) {
        byte[] bytea = comprimeAndTransformImageToBytea(path);
        Connexion conn = new Connexion();
        SavedLogin savedLogin = getSavedLoginFromFile();
        try {
            conn.conectar();
            if (savedLogin.getRole().equals("user"))
                conn.updatePhotoUser(savedLogin.getUid(), bytea);
            else conn.updatePhotoBar(savedLogin.getUid(), bytea);
            updatePhotoInFile(bytea);
            imgProfile.setImageBitmap(BitmapFactory.decodeByteArray(bytea, 0, bytea.length));
        } catch (Exception e) {
            Snackbar.make(getView(), R.string.errorNotConnection, Snackbar.LENGTH_SHORT).show();
            e.printStackTrace();
        } finally {
            if (conn.isConnected()) conn.desconectar();
        }
    }

    private byte[] comprimeAndTransformImageToBytea(String path) {
        Bitmap b = BitmapFactory.decodeFile(path);

        int origWidth = b.getWidth();
        int origHeight = b.getHeight();
        float destWidth;
        float destHeight;
        if (origWidth >= origHeight) {
            destHeight = 450;
            destWidth = origWidth / (origHeight / destHeight);
        } else {
            destWidth = 450;
            destHeight = origHeight / (origWidth / destWidth);
        }
        Bitmap b2 = Bitmap.createScaledBitmap(b, Math.round(destWidth), Math.round(destHeight), false);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        b2.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        return stream.toByteArray();
    }

    private void updatePhotoInFile(byte[] foto) {
        if (getSavedLoginFromFile().getRole().equals("user")) {
            User user = getUserFromFile();
            user.setFoto(foto);
            saveFileUserData(user);
        } else {
            Bar bar = getBarFromFile();
            bar.setFoto(foto);
            saveFileBarData(bar);
        }
    }

    private void showRequestPermissionsDialog(final int idRequest) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage(R.string.dialogPermissionsProfile)
                .setPositiveButton(R.string.btnRequestPermissions, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (idRequest == CAMERA_MULTIPLE_PERMISSIONS) requestCamera();
                        else if (idRequest == GALLERY_MULTIPLE_PERMISSIONS) requestGallery();
                    }
                })
                .setNegativeButton(R.string.btnCancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == CAMERA_REQUEST) {
                updatePhotoProfile(PHOTO_CAMERA);
            } else if (requestCode == GALLERY_REQUEST) {
                Uri selectedImage = data.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor c = getContext().getContentResolver().query(selectedImage, filePath, null, null, null);
                c.moveToFirst();
                String picturePath = c.getString(c.getColumnIndex(filePath[0]));
                c.close();
                updatePhotoProfile(picturePath);
            }
        }
    }

}