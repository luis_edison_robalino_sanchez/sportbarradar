package sbr.m15.dam.local.sportbarradar.viewHolder;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import sbr.m15.dam.local.sportbarradar.R;
import sbr.m15.dam.local.sportbarradar.model.Bar;

public class BaresViewHolder extends RecyclerView.ViewHolder {

    protected final int ICON_ACTIVATE = R.color.colorPrimaryDark;
    protected final int ICON_DESACTIVATE = R.color.colorAccent;

    @BindView(R.id.nameBarUser)
    TextView nameBarUser;
    @BindView(R.id.ratingValoration)
    RatingBar ratingValoration;
    @BindView(R.id.distanceBar)
    TextView distanceBar;
    @BindView(R.id.wifi)
    ImageView wifi;
    @BindView(R.id.terrace)
    ImageView terrace;
    @BindView(R.id.cardBar)
    public CardView cardBar;

    Context context;

    public BaresViewHolder(View itemView) {
        super(itemView);
        View rootView = itemView;
        context = itemView.getContext();
        ButterKnife.bind(this, rootView);
    }

    public void bindBar(final Bar b) {
        nameBarUser.setText(b.getNombre());
        distanceBar.setText((b.getDistancia() >= 0) ?
                b.getDistancia() + " " + context.getString(R.string.meters) : "");
        if (b.getValoracion() == -1) ratingValoration.setVisibility(View.GONE);
        else ratingValoration.setRating(b.getValoracion());
        wifi.setColorFilter((b.getWifi()) ?
                ContextCompat.getColor(context, ICON_ACTIVATE) :
                ContextCompat.getColor(context, ICON_DESACTIVATE));
        terrace.setColorFilter((b.getTerraza()) ?
                ContextCompat.getColor(context, ICON_ACTIVATE) :
                ContextCompat.getColor(context, ICON_DESACTIVATE));

    }
}
