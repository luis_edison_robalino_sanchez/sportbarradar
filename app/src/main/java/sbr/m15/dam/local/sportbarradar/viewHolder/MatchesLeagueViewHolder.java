package sbr.m15.dam.local.sportbarradar.viewHolder;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import sbr.m15.dam.local.sportbarradar.R;
import sbr.m15.dam.local.sportbarradar.model.Match;
import sbr.m15.dam.local.sportbarradar.utils.DateUtils;

public class MatchesLeagueViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.logoLocal)
    ImageView logoLocal;
    @BindView(R.id.nombreLocal)
    TextView nombreLocal;
    @BindView(R.id.logoVisitante)
    ImageView logoVisitante;
    @BindView(R.id.nombreVisitante)
    TextView nombreVisitante;
    @BindView(R.id.logoLiga)
    ImageView logoLiga;
    @BindView(R.id.diaPartido)
    TextView diaPartido;
    @BindView(R.id.cardMatch)
    public CardView cardMatch;

    Context context;
    Match match;

    public MatchesLeagueViewHolder(View itemView) {
        super(itemView);
        context = itemView.getContext();
        ButterKnife.bind(this, itemView);
    }

    public void bindMatches(Match m) {
        match = m;
        nombreLocal.setText(m.getLocal());
        nombreVisitante.setText(m.getVisitor());
        diaPartido.setText(m.getCompleteDate().contains("/")?
                DateUtils.formatDate(m.getCompleteDate()):
                DateUtils.formatDateEsp(m.getCompleteDate()));
        Glide.with(context)
                .load(m.getLocalShield())
                .crossFade()
                .into(logoLocal);
        Glide.with(context)
                .load(m.getVisitorShield())
                .crossFade()
                .into(logoVisitante);
        Glide.with(context)
                .load(m.getLeagueShield())
                .crossFade()
                .into(logoLiga);
    }
}
