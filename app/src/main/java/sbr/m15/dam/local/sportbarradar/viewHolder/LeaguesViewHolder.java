package sbr.m15.dam.local.sportbarradar.viewHolder;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import sbr.m15.dam.local.sportbarradar.R;
import sbr.m15.dam.local.sportbarradar.model.League;

public class LeaguesViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.logoLiga)
    ImageView logoLiga;
    @BindView(R.id.nombreLiga)
    TextView nombreLiga;
    @BindView(R.id.cardLeague)
    public CardView cardLeague;

    Context context;

    public LeaguesViewHolder(View itemView) {
        super(itemView);
        View rootView = itemView;
        context = itemView.getContext();
        ButterKnife.bind(this, rootView);
    }

    public void bindLeague(final League l) {
        nombreLiga.setText(l.getNombre());
        Glide.with(context)
                .load(l.getLogoUrl())
                .crossFade()
                .into(logoLiga);
    }
}
