package sbr.m15.dam.local.sportbarradar.userScreens;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import butterknife.BindView;
import sbr.m15.dam.local.sportbarradar.R;
import sbr.m15.dam.local.sportbarradar.adapter.MatchesLeagueAdapter;
import sbr.m15.dam.local.sportbarradar.bases.BaseFragment;
import sbr.m15.dam.local.sportbarradar.db.Connexion;
import sbr.m15.dam.local.sportbarradar.model.Match;

public class UserViewBarListMatchesFragment extends BaseFragment {

    @BindView(R.id.listadoPartidos)
    RecyclerView listadoPartidos;

    private MatchesLeagueAdapter adapter;
    private ArrayList<Match> matches;

    public UserViewBarListMatchesFragment() {
    }

    public static UserViewBarListMatchesFragment newInstance() {
        UserViewBarListMatchesFragment fragment = new UserViewBarListMatchesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        showMatchesList();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_list_matches, container, false);
    }

    private void showMatchesList() {
        matches = new ArrayList<Match>();
        Connexion conn = new Connexion();
        try {
            conn.conectar();
            matches = conn.getMatchesFromBar(getActivity().getIntent().getExtras().getString("uidBar"));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conn.desconectar();
        }
        adapter = new MatchesLeagueAdapter(matches);
        listadoPartidos.setAdapter(adapter);
        listadoPartidos.setLayoutManager(
                new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
    }

}
