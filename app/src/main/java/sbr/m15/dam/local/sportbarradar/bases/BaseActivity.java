package sbr.m15.dam.local.sportbarradar.bases;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;

import butterknife.ButterKnife;
import sbr.m15.dam.local.sportbarradar.R;
import sbr.m15.dam.local.sportbarradar.barScreens.BarMainActivity;
import sbr.m15.dam.local.sportbarradar.homeScreens.view.LoginActivity;
import sbr.m15.dam.local.sportbarradar.model.Bar;
import sbr.m15.dam.local.sportbarradar.model.SavedLogin;
import sbr.m15.dam.local.sportbarradar.model.User;
import sbr.m15.dam.local.sportbarradar.userScreens.UserMainActivity;
import sbr.m15.dam.local.sportbarradar.utils.FileUtils;

public class BaseActivity extends AppCompatActivity {

    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 4;

    ProgressDialog progressDialog;
    private String filePathSavedLogin;
    private String filePathBar;
    private String filePathUser;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        filePathSavedLogin = this.getFilesDir().getPath() + "/savedLogin.dat";
        filePathBar = this.getFilesDir().getPath() + "/bar.dat";
        filePathUser = this.getFilesDir().getPath() + "/user.dat";
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        ButterKnife.bind(this);
    }

    public void showProgress() {
        progressDialog = new ProgressDialog(BaseActivity.this);
        progressDialog.setMessage("Cargando....");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
    }

    public void hideProgress() {
        if (progressDialog != null)
            progressDialog.dismiss();
    }

    protected void saveFileUserData(User user) {
        FileUtils.saveFileUserData(filePathUser, user);
    }

    protected void saveFileBarData(Bar bar) {
        FileUtils.saveFileBarData(filePathBar, bar);
    }

    protected SavedLogin getSavedLoginFromFile() {
        return FileUtils.getSavedLoginFromFile(filePathSavedLogin);
    }

    protected void deleteFileSavedLogin() {
        FileUtils.deleteFile(filePathSavedLogin);
    }

    protected void deleteFileUserData() {
        FileUtils.deleteFile(filePathUser);
    }

    protected void deleteFileBarData() {
        FileUtils.deleteFile(filePathBar);
    }

    protected void saveFileSavedLogin(SavedLogin savedLogin) {
        FileUtils.saveFileSavedLogin(filePathSavedLogin, savedLogin);
    }

    private ArrayList<String> getRequestPermissions() {
        String[] permissions = {
                Manifest.permission.CAMERA,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.INTERNET,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
                // Añadir más permisos si hace falta
        };

        ArrayList<String> listPermissions = new ArrayList<>();
        for (String permission : permissions)
            if (ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_DENIED)
                listPermissions.add(permission);

        return listPermissions;
    }

    protected void requestPermissionsAndRedirect() {
        ArrayList<String> listPermissionsNeeded = getRequestPermissions();
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(
                    new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
        } else {
            redirectUser();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS: {
                if (!getRequestPermissions().isEmpty()) showRequestPermissionsDialog();
                else redirectUser();
            }
        }
    }

    private void showRequestPermissionsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.dialogPermissionsSplashScreen)
                .setPositiveButton(R.string.btnRequestPermissions, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        requestPermissionsAndRedirect();
                    }
                })
                .setNegativeButton(R.string.btnCancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        redirectUser();
                    }
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    protected void redirectUser() {
        SavedLogin savedLogin = getSavedLoginFromFile();
        Intent intent;
        if (savedLogin.getRole().equals("user")) {
            intent = new Intent(this, UserMainActivity.class);
            startActivity(intent);
        } else if (savedLogin.getRole().equals("bar")) {
            intent = new Intent(this, BarMainActivity.class);
            startActivity(intent);
        } else {
            deleteFileSavedLogin();
            deleteFileUserData();
            deleteFileBarData();
            intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        }
        finish();
    }
}