package sbr.m15.dam.local.sportbarradar.model;

import java.io.Serializable;

public class User implements Serializable {
    private String uid;
    private String nombre;
    private String email;
    private String password;
    private String repeatPassword;
    private Team equipoFavorito;
    private byte[] foto;

    public User() {
    }

    public User(String nombre) {
        this.nombre = nombre;
    }

    public User(String uid, String nombre) {
        this.uid = uid;
        this.nombre = nombre;
    }

    public User(String uid, String nombre, byte[] foto) {
        this(uid, nombre);
        this.foto = foto;
    }

    public User(String uid, String nombre, byte[] foto, int idEquipo, String fotoEquipo, String nombreEquipo) {
        this(uid, nombre, foto);
        this.equipoFavorito = new Team(idEquipo, nombreEquipo, fotoEquipo);
        ;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRepeatPassword() {
        return repeatPassword;
    }

    public void setRepeatPassword(String repeatPassword) {
        this.repeatPassword = repeatPassword;
    }

    public Team getEquipoFavorito() {
        return equipoFavorito;
    }

    public void setEquipoFavorito(Team equipoFavorito) {
        this.equipoFavorito = equipoFavorito;
    }

    public byte[] getFoto() {
        return foto;
    }

    public void setFoto(byte[] foto) {
        this.foto = foto;
    }
}
