package sbr.m15.dam.local.sportbarradar.api;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;
import sbr.m15.dam.local.sportbarradar.model.Ligas;
import sbr.m15.dam.local.sportbarradar.model.MatchDay;
import sbr.m15.dam.local.sportbarradar.model.Teams;

public interface ResultadosFutbol {
    public final String BASE_URL = "http://apiclient.resultados-futbol.com/scripts/api/";

    @GET("api.php?key=fd163a46678ed58ea55956df50acd4a2&tz=Europe/Madrid&format=json&req=categories")
    Observable<Ligas> getLigas(@Query("country") String country);

    @GET("api.php?key=fd163a46678ed58ea55956df50acd4a2&tz=Europe/Madrid&format=json&req=matchs")
    Observable<MatchDay> getLeagueNextMatches(@Query("league") int league, @Query("round") int round);

    @GET("api.php?key=fd163a46678ed58ea55956df50acd4a2&tz=Europe/Madrid&format=json&req=teams&league=1&year=2017")
    Observable<Teams> getTeams();
}
