package sbr.m15.dam.local.sportbarradar.homeScreens.view;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.HttpException;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import sbr.m15.dam.local.sportbarradar.R;
import sbr.m15.dam.local.sportbarradar.adapter.TeamAdapter;
import sbr.m15.dam.local.sportbarradar.api.ResultadosFutbol;
import sbr.m15.dam.local.sportbarradar.bases.BaseActivity;
import sbr.m15.dam.local.sportbarradar.homeScreens.presenter.RegisterUserPresenterImpl;
import sbr.m15.dam.local.sportbarradar.homeScreens.presenter.interfaces.RegisterUserPresenter;
import sbr.m15.dam.local.sportbarradar.homeScreens.view.interfaces.RegisterUserView;
import sbr.m15.dam.local.sportbarradar.model.Team;
import sbr.m15.dam.local.sportbarradar.model.Teams;
import sbr.m15.dam.local.sportbarradar.model.User;
import sbr.m15.dam.local.sportbarradar.utils.FormUtils;

public class RegisterUserActivity extends BaseActivity implements RegisterUserView {

    @BindView(R.id.fullName)
    TextInputLayout fullName;
    @BindView(R.id.email)
    TextInputLayout email;
    @BindView(R.id.password)
    TextInputLayout password;
    @BindView(R.id.repeatPassword)
    TextInputLayout repeatPassword;
    @BindView(R.id.editTextFullName)
    EditText editTextFullName;
    @BindView(R.id.editTextEmail)
    EditText editTextEmail;
    @BindView(R.id.editTextPassword)
    EditText editTextPassword;
    @BindView(R.id.editTextRepeatPassword)
    EditText editTextRepeatPassword;
    @BindView(R.id.eqFavorito)
    Spinner eqFavorito;

    private User user;
    private RegisterUserPresenter presenter;
    public static final String BASE_URL = "http://apiclient.resultados-futbol.com/scripts/api/";
    private Subscription subscription;
    RxJavaCallAdapterFactory rxAdapter = RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io());
    Retrofit retrofit;
    ResultadosFutbol apiService;
    TeamAdapter adapter;
    Context ctx;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ctx = this;
        setContentView(R.layout.activity_register_user);
        ButterKnife.bind(this);
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(rxAdapter)
                .build();
        apiService = retrofit.create(ResultadosFutbol.class);
        presenter = new RegisterUserPresenterImpl(this);
        user = new User();
        hideProgress();
        setUpSpinner();
    }

    private void setUpSpinner() {
        Observable<Teams> call = apiService.getTeams();
        subscription = call
                .subscribeOn(Schedulers.io()) // optional if you do not wish to override the default behavior
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Subscriber<Teams>() {
                    @Override
                    public void onCompleted() {
                        eqFavorito.setAdapter(adapter);
                        eqFavorito.setSelection(0);
                    }

                    @Override
                    public void onError(Throwable e) {
                        // cast to retrofit.HttpException to get the response code
                        if (e instanceof HttpException) {
                            HttpException response = (HttpException) e;
                            e.printStackTrace();
                        }
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(Teams teams) {
                        ArrayList<Team> eq = new ArrayList<Team>();
                        Team t = new Team();
                        t.setFullName(getString(R.string.selectTeam));
                        eq.add(t);
                        eq.addAll(teams.getEquipos());
                        adapter = new TeamAdapter(ctx, R.layout.list_item_team, eq);
                    }
                });
    }

    @OnClick({R.id.btnCancel, R.id.btnRegister})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnCancel:
                if (!formIsEmpty()) {
                    showCancelDialog();
                } else {
                    finish();
                }
                break;
            case R.id.btnRegister:
                user.setEmail(editTextEmail.getText().toString());
                user.setNombre(editTextFullName.getText().toString());
                user.setPassword(editTextPassword.getText().toString());
                user.setRepeatPassword(editTextRepeatPassword.getText().toString());
                user.setEquipoFavorito((Team) eqFavorito.getSelectedItem());
                presenter.validateRegister(user);
                break;
        }
    }

    private boolean formIsEmpty() {
        if (FormUtils.isNotEmpty(editTextFullName.getText().toString())) return false;
        if (FormUtils.isNotEmpty(editTextEmail.getText().toString())) return false;
        if (FormUtils.isNotEmpty(editTextPassword.getText().toString())) return false;
        if (FormUtils.isNotEmpty(editTextRepeatPassword.getText().toString())) return false;
        return true;
    }

    @Override
    public void setErrorMail() {
        hideProgress();
        email.setError(getString(R.string.errorEmailExist));
    }

    @Override
    public void setErrorEmptyMail() {
        hideProgress();
        email.setError(getString(R.string.errorEmptyField));
    }

    @Override
    public void setErrorNotValidMail() {
        hideProgress();
        email.setError(getString(R.string.errorEmailNotValid));
    }

    @Override
    public void setErrorEmptyName() {
        hideProgress();
        fullName.setError(getString(R.string.errorEmptyField));
    }

    @Override
    public void setErrorEmptyPassword() {
        hideProgress();
        password.setError(getString(R.string.errorEmptyField));
    }

    @Override
    public void setErrorEmptyRepeatPassword() {
        hideProgress();
        repeatPassword.setError(getString(R.string.errorEmptyField));
    }

    @Override
    public void setErrorPasswordNotEqual() {
        hideProgress();
        password.setError(getString(R.string.errorPasswordNotEqual));
        repeatPassword.setError("");
    }

    @Override
    public void setErrorShortPassword() {
        hideProgress();
        password.setError(getString(R.string.errorShortPassword));
    }

    @Override
    public void setErrorPlayServicesException() {
        hideProgress();
        Snackbar.make(fullName, R.string.errorGoogleServices, Snackbar.LENGTH_LONG)
                .show();
    }

    @Override
    public void setErrorNotConnection() {
        hideProgress();
        Snackbar.make(fullName, R.string.errorNotConnection, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void setErrorTeam() {
        hideProgress();
        TextView errorText = (TextView) eqFavorito.getSelectedView().findViewById(R.id.nameTeam);
        String error = getString(R.string.selectTeam);
        errorText.setError(error);
        errorText.setTextColor(Color.RED);
        errorText.setText(error);
    }

    @Override
    public void cleanErrors() {
        hideProgress();
        fullName.setError("");
        email.setError("");
        password.setError("");
        repeatPassword.setError("");
    }

    @Override
    public void showCancelDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.dialogCancelRegister)
                .setPositiveButton(R.string.btnAccept, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                })
                .setNegativeButton(R.string.btnCancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public void redirect() {
        Intent i = new Intent(this, LoginActivity.class);
        i.putExtra("msgRegister", getString(R.string.ntfCorrectRegisterUser));
        startActivity(i);
        finish();
    }

    @Override
    public void onBackPressed() {
        if (!formIsEmpty()) showCancelDialog();
    }
}