package sbr.m15.dam.local.sportbarradar.utils;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Patterns;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseUser;

public class FormUtils {

    public static boolean isNotEmpty(String text) {
        if (TextUtils.isEmpty(text)) return false;
        return true;
    }

    public static boolean isEmailValid(String email) {
        //if (email.contains("@mailinator.com")) return false;
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static void deleteUserFirebase(FirebaseUser firebaseUser) {
        firebaseUser.delete()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                    }
                });
    }
}
