package sbr.m15.dam.local.sportbarradar.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import sbr.m15.dam.local.sportbarradar.R;
import sbr.m15.dam.local.sportbarradar.model.Match;
import sbr.m15.dam.local.sportbarradar.viewHolder.MatchesLeagueViewHolder;

public class MatchesLeagueLongAdapter extends RecyclerView.Adapter<MatchesLeagueViewHolder> implements View.OnLongClickListener {

    ArrayList<Match> matches;
    private View.OnLongClickListener listenerLong;

    public MatchesLeagueLongAdapter(ArrayList<Match> matches) {
        this.matches = matches;
    }

    @Override
    public MatchesLeagueViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_matches_bar, parent, false);
        itemView.setOnLongClickListener(this);
        MatchesLeagueViewHolder vvh = new MatchesLeagueViewHolder(itemView);
        return vvh;
    }

    @Override
    public void onBindViewHolder(MatchesLeagueViewHolder holder, int position) {
        holder.bindMatches(matches.get(position));
    }

    @Override
    public int getItemCount() {
        return matches.size();
    }

    public void setLongOnClickListener(View.OnLongClickListener listener) {
        this.listenerLong = listener;
    }

    @Override
    public boolean onLongClick(View view) {
        if (listenerLong != null)
            listenerLong.onLongClick(view);
        return true;
    }
}
