package sbr.m15.dam.local.sportbarradar.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import sbr.m15.dam.local.sportbarradar.R;
import sbr.m15.dam.local.sportbarradar.model.Match;
import sbr.m15.dam.local.sportbarradar.viewHolder.MatchesViewHolder;

public class MatchesAdapter extends RecyclerView.Adapter<MatchesViewHolder> {

    ArrayList<Match> matches;
    MatchesItemClickListener listener;

    public MatchesAdapter(ArrayList<Match> matches, MatchesItemClickListener l) {
        this.matches = matches;
        listener = l;
    }

    public static interface MatchesItemClickListener {
        void itemClicked(Match m);
    }


    @Override
    public MatchesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_matches, parent, false);
        MatchesViewHolder lvh = new MatchesViewHolder(itemView);
        return lvh;
    }

    @Override
    public void onBindViewHolder(MatchesViewHolder holder, final int position) {
        holder.bindMatch(matches.get(position));
        holder.cardMatch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.itemClicked(matches.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return matches.size();
    }

}
