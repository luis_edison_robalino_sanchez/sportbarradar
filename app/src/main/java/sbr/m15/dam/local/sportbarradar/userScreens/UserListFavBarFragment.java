package sbr.m15.dam.local.sportbarradar.userScreens;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.ArrayList;

import butterknife.BindView;
import sbr.m15.dam.local.sportbarradar.R;
import sbr.m15.dam.local.sportbarradar.adapter.BaresAdapter;
import sbr.m15.dam.local.sportbarradar.bases.LocationBaseFragment;
import sbr.m15.dam.local.sportbarradar.db.Connexion;
import sbr.m15.dam.local.sportbarradar.model.Bar;

public class UserListFavBarFragment extends LocationBaseFragment implements BaresAdapter.BarItemClickListener {

    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.listadoBares)
    RecyclerView listadoBares;

    ArrayList<Bar> bares;
    private BaresAdapter bAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    public UserListFavBarFragment() {
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        progressBar.setVisibility(View.VISIBLE);
        bares = new ArrayList<Bar>();
        Connexion conn = new Connexion();
        try {
            conn.conectar();
            if (latitude != 0 && longitude != 0)
                bares = conn.getFavoriteBar(getUidFromFile(), latitude, longitude);
            else bares = conn.getFavoriteBar(getUidFromFile());
        } catch (Exception e) {
            Snackbar.make(getView(), R.string.errorNotConnection, Snackbar.LENGTH_SHORT).show();
            e.printStackTrace();
        } finally {
            if (conn.isConnected()) conn.desconectar();
        }
        bAdapter = new BaresAdapter(bares, getContext(), this);
        mLayoutManager = new LinearLayoutManager(getContext());
        listadoBares.setLayoutManager(mLayoutManager);
        listadoBares.setAdapter(bAdapter);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_user_list_fav_bar, container, false);
    }

    public static UserListFavBarFragment newInstance() {
        UserListFavBarFragment fragment = new UserListFavBarFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void itemClicked(Bar b) {
        showDialogConfirmRedirectToMap(b);
    }
}
