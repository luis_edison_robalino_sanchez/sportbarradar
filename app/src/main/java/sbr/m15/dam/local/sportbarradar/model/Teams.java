package sbr.m15.dam.local.sportbarradar.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Teams {
    @SerializedName("team")
    @Expose
    private List<Team> equipos = null;

    public List<Team> getEquipos() {
        return equipos;
    }

    public void setEquipos(List<Team> equipos) {
        this.equipos = equipos;
    }
}
