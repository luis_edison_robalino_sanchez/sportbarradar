package sbr.m15.dam.local.sportbarradar.homeScreens.presenter.interfaces;

import sbr.m15.dam.local.sportbarradar.model.Bar;

public interface RegisterBarPresenter {
    void validateRegister(Bar bar);

    void onDestroy();
}
