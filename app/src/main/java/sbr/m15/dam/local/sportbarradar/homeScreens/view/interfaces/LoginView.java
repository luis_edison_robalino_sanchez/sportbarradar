package sbr.m15.dam.local.sportbarradar.homeScreens.view.interfaces;

import sbr.m15.dam.local.sportbarradar.model.Bar;
import sbr.m15.dam.local.sportbarradar.model.User;

public interface LoginView {
    void setErrorMail();

    void setErrorEmptyMail();

    void setErrorNotValidMail();

    void setErrorPassword();

    void setErrorEmptyPassword();

    void register();

    void showProgress();

    void hideProgress();

    void forgotPassword();

    void redirect(Class home);

    void redirectNotFinish(Class home);

    void setErrorNotConnection();

    void cleanErrors();

    void setErrorRecoveryMail();

    void onRecoveryMailSuccessful();

    void onRecoveryNotConnection();

    void saveSavedData(String uid, String role, String email);

    void saveUserData(User user);

    void saveBarData(Bar bar);
}
