package sbr.m15.dam.local.sportbarradar.homeScreens.view.interfaces;

public interface RegisterUserView {
    void setErrorMail();

    void setErrorEmptyMail();

    void setErrorNotValidMail();

    void setErrorEmptyName();

    void setErrorEmptyPassword();

    void setErrorEmptyRepeatPassword();

    void setErrorPasswordNotEqual();

    void setErrorShortPassword();

    void showProgress();

    void hideProgress();

    void cleanErrors();

    void showCancelDialog();

    void setErrorPlayServicesException();

    void redirect();

    void setErrorNotConnection();

    void setErrorTeam();
}
