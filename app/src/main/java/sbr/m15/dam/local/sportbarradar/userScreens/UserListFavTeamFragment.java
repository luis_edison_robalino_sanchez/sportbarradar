package sbr.m15.dam.local.sportbarradar.userScreens;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.ArrayList;

import butterknife.BindView;
import sbr.m15.dam.local.sportbarradar.R;
import sbr.m15.dam.local.sportbarradar.adapter.MatchesLeagueAdapter;
import sbr.m15.dam.local.sportbarradar.bases.BaseFragment;
import sbr.m15.dam.local.sportbarradar.db.Connexion;
import sbr.m15.dam.local.sportbarradar.model.Match;

public class UserListFavTeamFragment extends BaseFragment implements MatchesLeagueAdapter.MatchesLeagueItemClickListener {

    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.listadoPartidos)
    RecyclerView listadoPartidos;

    ArrayList<Match> partidos;
    private MatchesLeagueAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private UserListBaresFromMatchFragment barFragment;

    public UserListFavTeamFragment() {
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        progressBar.setVisibility(View.VISIBLE);
        partidos = new ArrayList<Match>();
        Connexion conn = new Connexion();
        try {
            conn.conectar();
            partidos = conn.getMatchesFromTeam(getUserFromFile().getEquipoFavorito().getFullName());
        } catch (Exception e) {
            Snackbar.make(getView(), R.string.errorNotConnection, Snackbar.LENGTH_SHORT).show();
            e.printStackTrace();
        } finally {
            if (conn.isConnected()) conn.desconectar();
        }
        mAdapter = new MatchesLeagueAdapter(partidos, this);
        mLayoutManager = new LinearLayoutManager(getContext());
        listadoPartidos.setLayoutManager(mLayoutManager);
        listadoPartidos.setAdapter(mAdapter);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_user_list_fav_team, container, false);
    }

    public static UserListFavTeamFragment newInstance() {
        UserListFavTeamFragment fragment = new UserListFavTeamFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void itemClicked(Match m) {
        barFragment = UserListBaresFromMatchFragment.newInstance(m);
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.contenedorDeBares, barFragment, "baresDeXPartido")
                .commit();
    }
}
