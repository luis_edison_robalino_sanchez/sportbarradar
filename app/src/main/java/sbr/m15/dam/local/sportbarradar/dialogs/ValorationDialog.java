package sbr.m15.dam.local.sportbarradar.dialogs;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.Unbinder;
import sbr.m15.dam.local.sportbarradar.R;
import sbr.m15.dam.local.sportbarradar.adapter.ValorationsLongAdapter;
import sbr.m15.dam.local.sportbarradar.bases.BaseDialogFragment;
import sbr.m15.dam.local.sportbarradar.db.Connexion;
import sbr.m15.dam.local.sportbarradar.model.Valoration;

public class ValorationDialog extends BaseDialogFragment {

    protected Unbinder unbinder;
    @BindView(R.id.dialogRating)
    RatingBar dialogRating;
    @BindView(R.id.dialogEditTextValoration)
    TextInputEditText dialogEditTextValoration;

    private Connexion conn;
    private Valoration valoration;
    private View rootView;

    private String uidUser;
    private String uidBar;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rootView = getActivity().getWindow().getDecorView().getRootView();
        uidUser = getArguments().getString("uidUser");
        uidBar = getArguments().getString("uidBar");
        setValorationData();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_valoration, container, false);
    }

    private void setValorationData() {
        conn = new Connexion();
        try {
            conn.conectar();
            valoration = conn.getValoration(uidUser, getArguments().getString("uidBar"));
            if (valoration != null) {
                dialogRating.setRating(valoration.getPuntuation());
                dialogEditTextValoration.setText(valoration.getDescription());
            }
        } catch (Exception e) {
            Snackbar.make(rootView, R.string.errorNotConnection, Snackbar.LENGTH_SHORT).show();
            e.printStackTrace();
        } finally {
            if (conn.isConnected()) conn.desconectar();
        }
    }

    @OnClick({R.id.cancelValoration, R.id.saveValoration})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cancelValoration:
                dismiss();
                break;
            case R.id.saveValoration:
                if (valoration != null) updateValoration();
                else addValoration();
                Connexion conn = new Connexion();
                try {
                    conn.conectar();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (conn.isConnected()) conn.desconectar();
                }

                dismiss();
                break;
        }
    }

    private void updateValoration() {
        conn = new Connexion();
        try {
            conn.conectar();
            conn.updateValoration(new Valoration(uidUser, uidBar,
                    dialogEditTextValoration.getText().toString(), dialogRating.getRating()));
            Snackbar.make(rootView, R.string.msgUpdateValoration, Snackbar.LENGTH_SHORT).show();
            refreshAdapter(getArguments().getString("uid").equals(uidUser) ?
                    conn.getValorationsFromUser(uidUser) : conn.getValorationsFromBar(uidBar));
        } catch (Exception e) {
            Snackbar.make(rootView, R.string.errorNotConnection, Snackbar.LENGTH_SHORT).show();
            e.printStackTrace();
        } finally {
            if (conn.isConnected()) conn.desconectar();
        }
    }

    private void refreshAdapter(ArrayList<Valoration> valorations) {
        ValorationsLongAdapter adapter = (ValorationsLongAdapter) getArguments().getSerializable("adapter");
        adapter.refreshData(valorations);
    }

    private void addValoration() {
        conn = new Connexion();
        try {
            conn.conectar();
            conn.createValoration(new Valoration(uidUser, uidBar,
                    dialogEditTextValoration.getText().toString(), dialogRating.getRating()));
            Snackbar.make(rootView, R.string.msgAddValoration, Snackbar.LENGTH_SHORT).show();
            refreshAdapter(getArguments().getString("uid").equals(uidUser) ?
                    conn.getValorationsFromUser(uidUser) : conn.getValorationsFromBar(uidBar));
        } catch (Exception e) {
            Snackbar.make(rootView, R.string.errorNotConnection, Snackbar.LENGTH_SHORT).show();
            e.printStackTrace();
        } finally {
            if (conn.isConnected()) conn.desconectar();
        }
    }
}
