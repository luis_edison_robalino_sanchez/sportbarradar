package sbr.m15.dam.local.sportbarradar.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Spain implements Serializable {
    @SerializedName("ligas")
    @Expose
    private List<League> ligas = null;
    @SerializedName("competiciones")
    @Expose
    private List<League> competiciones = null;

    public List<League> getLigas() {
        return ligas;
    }

    public void setLigas(List<League> ligas) {
        this.ligas = ligas;
    }

    public List<League> getCompeticiones() {
        return competiciones;
    }

    public void setCompeticiones(List<League> competiciones) {
        this.competiciones = competiciones;
    }
}