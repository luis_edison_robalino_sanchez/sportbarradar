package sbr.m15.dam.local.sportbarradar.userScreens;

import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.siyamed.shapeimageview.mask.PorterShapeImageView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import sbr.m15.dam.local.sportbarradar.R;
import sbr.m15.dam.local.sportbarradar.adapter.ValorationsAdapter;
import sbr.m15.dam.local.sportbarradar.bases.BaseFragment;
import sbr.m15.dam.local.sportbarradar.db.Connexion;
import sbr.m15.dam.local.sportbarradar.dialogs.ValorationDialog;
import sbr.m15.dam.local.sportbarradar.model.Bar;
import sbr.m15.dam.local.sportbarradar.model.Valoration;

public class UserViewBarProfileFragment extends BaseFragment {

    @BindView(R.id.txtUsername)
    TextView txtUsername;
    @BindView(R.id.listValorations)
    RecyclerView listValorations;
    @BindView(R.id.wifi)
    ImageView wifi;
    @BindView(R.id.terrace)
    ImageView terrace;
    @BindView(R.id.fav)
    ImageView fav;
    @BindView(R.id.imgProfile)
    PorterShapeImageView imgProfile;

    private ValorationsAdapter adapter;
    private ArrayList<Valoration> valorations;
    private Bar barData;
    private String uidBar;
    private boolean userIsFavOfTheBar;

    public UserViewBarProfileFragment() {
    }

    public static UserViewBarProfileFragment newInstance() {
        UserViewBarProfileFragment fragment = new UserViewBarProfileFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        uidBar = getActivity().getIntent().getExtras().getString("uidBar");
        Connexion conn = new Connexion();
        try {
            conn.conectar();
            barData = conn.getBar(uidBar);
            loadProfile();
        } catch (Exception e) {
            Snackbar.make(getView(), R.string.errorNotConnection, Snackbar.LENGTH_SHORT).show();
            e.printStackTrace();
        } finally {
            conn.desconectar();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile_another, container, false);
    }

    private void showValorationsList() {
        valorations = new ArrayList<Valoration>();
        Connexion conn = new Connexion();
        try {
            conn.conectar();
            valorations = conn.getValorationsFromBar(barData.getUid());
        } catch (Exception e) {
            Snackbar.make(getView(), R.string.errorNotConnection, Snackbar.LENGTH_SHORT).show();
            e.printStackTrace();
        } finally {
            conn.desconectar();
        }
        adapter = new ValorationsAdapter(valorations);
        listValorations.setAdapter(adapter);
        listValorations.setLayoutManager(
                new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
    }

    @OnClick(R.id.menuConfig)
    public void onViewClicked(View view) {
        showMenu(view);
    }

    private void loadProfile() {
        txtUsername.setText(barData.getNombre());
        loadImageProfile();
        loadWifiTerrace();
        loadFav();
        showValorationsList();
    }

    private void loadImageProfile() {
        byte[] foto = barData.getFoto();
        if (foto != null) {
            imgProfile.setImageBitmap(BitmapFactory.decodeByteArray(foto, 0, foto.length));
        }
    }

    private void loadWifiTerrace() {
        wifi.setVisibility(View.VISIBLE);
        terrace.setVisibility(View.VISIBLE);
        Connexion conn = new Connexion();
        try {
            conn.conectar();
            Bar b = conn.getWifiTerraceFromBar(barData.getUid());
            barData.setWifi(b.getWifi());
            barData.setTerraza(b.getTerraza());
            wifi.setColorFilter((barData.getWifi()) ?
                    ContextCompat.getColor(getContext(), ICON_ACTIVATE) :
                    ContextCompat.getColor(getContext(), ICON_DESACTIVATE));
            terrace.setColorFilter((barData.getTerraza()) ?
                    ContextCompat.getColor(getContext(), ICON_ACTIVATE) :
                    ContextCompat.getColor(getContext(), ICON_DESACTIVATE));
        } catch (Exception e) {
            Snackbar.make(getView(), R.string.errorNotConnection, Snackbar.LENGTH_SHORT).show();
            e.printStackTrace();
        } finally {
            conn.desconectar();
        }
    }

    private void loadFav() {
        Connexion conn = new Connexion();
        try {
            conn.conectar();
            userIsFavOfTheBar = conn.userIsFavOfTheBar(getUidFromFile(), barData.getUid());
            if (!userIsFavOfTheBar) fav.setColorFilter(
                    ContextCompat.getColor(getContext(), ICON_DESACTIVATE));
        } catch (Exception e) {
            Snackbar.make(getView(), R.string.errorNotConnection, Snackbar.LENGTH_SHORT).show();
            e.printStackTrace();
        } finally {
            conn.desconectar();
        }
    }

    private void showMenu(View view) {
        PopupMenu popup = new PopupMenu(getContext(), view);
        popup.getMenuInflater().inflate(R.menu.menu_see_profile_bar, popup.getMenu());
        changeTitleMenu(popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menuValoration:
                        editValorationDialog(getUidFromFile(), barData.getUid());
                        break;
                    case R.id.menuChangeFavBar:
                        changeStateFav();
                        break;
                    case R.id.menuReturnToMap:
                        getActivity().onBackPressed();
                        break;
                }
                return true;
            }
        });
        popup.show();
    }

    private void changeTitleMenu(Menu menu) {
        menu.getItem(1).setTitle((userIsFavOfTheBar) ?
                R.string.menuFavBarDesactivate : R.string.menuFavBarActivate);
    }

    private void changeStateFav() {
        Connexion conn = new Connexion();
        try {
            conn.conectar();
            if (userIsFavOfTheBar) conn.deleteFavoriteBar(getUidFromFile(), barData.getUid());
            else conn.createFavoriteBar(getUidFromFile(), barData.getUid());
            fav.setColorFilter((userIsFavOfTheBar) ?
                    ContextCompat.getColor(getContext(), ICON_DESACTIVATE) :
                    ContextCompat.getColor(getContext(), ICON_ACTIVATE));
            userIsFavOfTheBar = !userIsFavOfTheBar;
        } catch (Exception e) {
            Snackbar.make(getView(), R.string.errorNotConnection, Snackbar.LENGTH_SHORT).show();
            e.printStackTrace();
        } finally {
            conn.desconectar();
        }
    }

    protected void editValorationDialog(String uidUser, String uidBar) {
        FragmentManager fragmentManager = getFragmentManager();
        ValorationDialog newFragment = new ValorationDialog();
        Bundle args = new Bundle();
        args.putString("uid", uidBar);
        args.putSerializable("adapter", adapter);
        args.putString("uidUser", uidUser);
        args.putString("uidBar", uidBar);
        newFragment.setArguments(args);
        newFragment.setCancelable(false);
        newFragment.show(fragmentManager, "dialog");
    }
}
