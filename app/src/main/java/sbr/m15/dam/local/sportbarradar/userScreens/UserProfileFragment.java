package sbr.m15.dam.local.sportbarradar.userScreens;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import sbr.m15.dam.local.sportbarradar.R;
import sbr.m15.dam.local.sportbarradar.adapter.ValorationsLongAdapter;
import sbr.m15.dam.local.sportbarradar.bases.ProfileBaseFragment;
import sbr.m15.dam.local.sportbarradar.db.Connexion;
import sbr.m15.dam.local.sportbarradar.dialogs.ChangeFavTeamDialog;
import sbr.m15.dam.local.sportbarradar.dialogs.ValorationDialog;
import sbr.m15.dam.local.sportbarradar.model.User;
import sbr.m15.dam.local.sportbarradar.model.Valoration;

public class UserProfileFragment extends ProfileBaseFragment {

    @BindView(R.id.txtUsername)
    TextView txtUsername;
    @BindView(R.id.listValorations)
    RecyclerView listValorations;
    @BindView(R.id.wifi)
    ImageView wifi;

    private ValorationsLongAdapter adapter;
    private ArrayList<Valoration> valorations;
    private User userData;

    public UserProfileFragment() {
    }

    public static UserProfileFragment newInstance() {
        UserProfileFragment fragment = new UserProfileFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        userData = getUserFromFile();
        txtUsername.setText(userData.getNombre());
        Glide.with(this).load(userData.getEquipoFavorito().getShield()).into(wifi);
        wifi.setVisibility(View.VISIBLE);
        showValorationsList();
    }

    private void showValorationsList() {
        valorations = new ArrayList<Valoration>();
        Connexion conn = new Connexion();
        try {
            conn.conectar();
            valorations = conn.getValorationsFromUser(getUidFromFile());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conn.isConnected()) conn.desconectar();
        }
        adapter = new ValorationsLongAdapter(valorations);
        adapter.setLongOnClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                int pos = listValorations.getChildAdapterPosition(v);
                Valoration valoration = valorations.get(pos);
                editValorationDialog(valoration.getUidUser(), valoration.getUidBar());
                return true;
            }
        });
        listValorations.setAdapter(adapter);
        listValorations.setLayoutManager(
                new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
    }

    protected void editValorationDialog(String uidUser, String uidBar) {
        FragmentManager fragmentManager = getFragmentManager();
        ValorationDialog newFragment = new ValorationDialog();
        Bundle args = new Bundle();
        args.putString("uid", getUidFromFile());
        args.putSerializable("adapter", adapter);
        args.putString("uidUser", uidUser);
        args.putString("uidBar", uidBar);
        newFragment.setArguments(args);
        newFragment.setCancelable(false);
        newFragment.show(fragmentManager, "dialog");
    }

    @OnClick({R.id.menuConfig, R.id.imgProfile})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.menuConfig:
                showMenu(view);
                break;
            case R.id.imgProfile:
                changePhotoProfileDialog();
                break;
        }
    }

    private void showMenu(View view) {
        PopupMenu popup = new PopupMenu(getContext(), view);
        popup.getMenuInflater().inflate(R.menu.menu_profile_user, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menuChangeFavTeam:
                        changeFavoriteTeamDialog();
                        break;
                    case R.id.menuChangePassword:
                        changePasswordDialog();
                        break;
                    case R.id.menuLogout:
                        logout();
                        break;
                }
                return true;
            }
        });
        popup.show();
    }

    private void changeFavoriteTeamDialog() {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        ChangeFavTeamDialog newFragment = new ChangeFavTeamDialog();
        Bundle args = new Bundle();
        args.putString("uidUser", getUidFromFile());
        newFragment.setArguments(args);
        newFragment.setCancelable(false);
        newFragment.show(fragmentManager, "dialog");
    }

}
