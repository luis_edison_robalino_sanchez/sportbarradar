package sbr.m15.dam.local.sportbarradar.db;

import android.os.StrictMode;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import sbr.m15.dam.local.sportbarradar.model.Bar;
import sbr.m15.dam.local.sportbarradar.model.League;
import sbr.m15.dam.local.sportbarradar.model.Match;
import sbr.m15.dam.local.sportbarradar.model.Team;
import sbr.m15.dam.local.sportbarradar.model.User;
import sbr.m15.dam.local.sportbarradar.model.Valoration;

public class Connexion {
    Connection conn;
    Thread sqlThread = new Thread() {
        public void run() {
            try {
                Class.forName("org.postgresql.Driver");
                conn = DriverManager.getConnection("jdbc:postgresql://192.168.0.21:5432/rsb",
                        "hooper", "grace");
            } catch (SQLException e) {
                System.out.println("Fallo al conectar: " + e.toString().trim());
                conn = null;
            } catch (ClassNotFoundException e) {
                conn = null;
                System.out.println("No cargado el driver");
            }
        }
    };

    public void conectar() throws Exception {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        sqlThread.start();
        sqlThread.join(3000);
    }

    public void desconectar() {
        try {
            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isConnected() {
        return null != conn;
    }

    /* USUARIO */

    public void createUsuario(User user) {
        PreparedStatement stmt = null;
        try {
            String insertTableSQL = "INSERT INTO usuario (uid, nombre_completo, id_equipo, " +
                    "img_equipo, nombre_equipo) VALUES(?,?,?,?,?)";
            stmt = conn.prepareStatement(insertTableSQL);
            stmt.setString(1, user.getUid());
            stmt.setString(2, user.getNombre());
            stmt.setInt(3, user.getEquipoFavorito().getId());
            stmt.setString(4, user.getEquipoFavorito().getShield());
            stmt.setString(5, user.getEquipoFavorito().getFullName());
            stmt.executeUpdate();
            stmt.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    public Boolean existUserFromFullName(String fullName) {
        PreparedStatement stmt = null;
        Boolean exist = false;
        try {
            String selectTableSQL = "SELECT uid FROM usuario WHERE nombre_completo = ?";
            stmt = conn.prepareStatement(selectTableSQL);
            stmt.setString(1, fullName);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) exist = true;
            rs.close();
            stmt.close();
        } catch (SQLException se) {
            se.printStackTrace();
            System.out.println(se.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        return exist;
    }

    public User getUsuario(String uid) {
        User u = null;
        PreparedStatement stmt = null;
        try {
            String selectTableSQL = "SELECT uid, nombre_completo, foto, id_equipo, img_equipo, " +
                    "nombre_equipo FROM usuario WHERE uid = ?";
            stmt = conn.prepareStatement(selectTableSQL);
            stmt.setString(1, uid);
            ResultSet rs = stmt.executeQuery();
            while (rs.next())
                u = new User(rs.getString(1), rs.getString(2), rs.getBytes(3), rs.getInt(4),
                        rs.getString(5), rs.getString(6));
            rs.close();
            stmt.close();
        } catch (SQLException se) {
            se.printStackTrace();
            System.out.println(se.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        return u;
    }

    public byte[] updatePhotoUser(String uid, byte[] bytea) {
        byte[] foto = null;
        PreparedStatement stmt = null;
        try {
            String updateTableSQL = "UPDATE usuario SET foto=? WHERE uid=?";
            stmt = conn.prepareStatement(updateTableSQL);
            stmt.setBytes(1, bytea);
            stmt.setString(2, uid);
            stmt.executeUpdate();
            stmt.close();
            String selectTableSQL = "SELECT foto FROM usuario WHERE uid = ?";
            stmt = conn.prepareStatement(selectTableSQL);
            stmt.setString(1, uid);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) foto = rs.getBytes(1);
            rs.close();
            stmt.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        return foto;
    }

    public void updateFavTeam(String uid, Team team) {
        PreparedStatement stmt = null;
        try {
            String updateTableSQL = "UPDATE usuario SET id_equipo=?, nombre_equipo=?, " +
                    "img_equipo=? WHERE uid=?";
            stmt = conn.prepareStatement(updateTableSQL);
            stmt.setInt(1, team.getId());
            stmt.setString(2, team.getFullName());
            stmt.setString(3, team.getShield());
            stmt.setString(4, uid);
            stmt.executeUpdate();
            stmt.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    public void deleteUsuarioFromFullName(String fullName) {
        PreparedStatement stmt = null;
        try {
            String deleteTableSQL = "DELETE FROM usuario WHERE nombre_completo = ?";
            stmt = conn.prepareStatement(deleteTableSQL);
            stmt.setString(1, fullName);
            stmt.executeQuery();
            stmt.close();
        } catch (SQLException se) {
            se.printStackTrace();
            System.out.println(se.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    /* BAR */

    public void createBar(Bar bar) {
        PreparedStatement stmt = null;
        try {
            String insertTableSQL = "INSERT INTO bar VALUES(?,?,?,?,?,?)";
            stmt = conn.prepareStatement(insertTableSQL);
            stmt.setString(1, bar.getUid());
            stmt.setString(2, bar.getNombre());
            stmt.setDouble(3, bar.getLat());
            stmt.setDouble(4, bar.getLon());
            stmt.setBoolean(5, bar.getWifi());
            stmt.setBoolean(6, bar.getTerraza());
            stmt.executeUpdate();
            stmt.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    public Bar getBar(String uid) {
        Bar b = null;
        PreparedStatement stmt = null;
        try {
            String selectTableSQL = "SELECT uid, nombre, lat, lon, wifi, terraza, foto FROM bar " +
                    "WHERE uid = ?";
            stmt = conn.prepareStatement(selectTableSQL);
            stmt.setString(1, uid);
            ResultSet rs = stmt.executeQuery();
            while (rs.next())
                b = new Bar(rs.getString(1), rs.getString(2), rs.getFloat(3), rs.getFloat(4),
                        rs.getBoolean(5), rs.getBoolean(6), rs.getBytes(7));
            rs.close();
            stmt.close();
        } catch (SQLException se) {
            se.printStackTrace();
            System.out.println(se.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        return b;
    }

    public ArrayList<Bar> getAllBar() {
        ArrayList<Bar> bares = new ArrayList<Bar>();
        PreparedStatement stmt = null;
        try {
            String selectTableSQL = "SELECT uid, nombre, lat, lon, wifi, terraza, foto, " +
                    "AVG(valoracion) FROM bar FULL OUTER JOIN valoracion ON (uid = uid_bar) " +
                    "GROUP BY uid";
            stmt = conn.prepareStatement(selectTableSQL);
            ResultSet rs = stmt.executeQuery();
            while (rs.next())
                bares.add(new Bar(rs.getString(1), rs.getString(2), rs.getFloat(3), rs.getFloat(4),
                        rs.getBoolean(5), rs.getBoolean(6), rs.getBytes(7),
                        (rs.getString(8) != null) ? rs.getFloat(8) : -1, -1));
            rs.close();
            stmt.close();
        } catch (SQLException se) {
            se.printStackTrace();
            System.out.println(se.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        return bares;
    }

    public ArrayList<Bar> getBaresFromMatch(int api) {
        ArrayList<Bar> bares = new ArrayList<Bar>();
        PreparedStatement stmt = null;
        try {
            String selectTableSQL = "SELECT uid, nombre, lat, lon, wifi, terraza, foto, " +
                    "AVG(valoracion) FROM bar JOIN partido p ON (uid = p.uid_bar) " +
                    "FULL OUTER JOIN valoracion v ON (uid = v.uid_bar) " +
                    "WHERE api=? GROUP BY uid";
            stmt = conn.prepareStatement(selectTableSQL);
            stmt.setInt(1, api);
            ResultSet rs = stmt.executeQuery();
            while (rs.next())
                bares.add(new Bar(rs.getString(1), rs.getString(2), rs.getFloat(3), rs.getFloat(4),
                        rs.getBoolean(5), rs.getBoolean(6), rs.getBytes(7),
                        (rs.getString(8) != null) ? rs.getFloat(8) : -1, -1));
            rs.close();
            stmt.close();
        } catch (SQLException se) {
            se.printStackTrace();
            System.out.println(se.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        return bares;
    }

    public ArrayList<Bar> getBaresFromMatch(int api, double lat, double lon) {
        ArrayList<Bar> bares = new ArrayList<Bar>();
        PreparedStatement stmt = null;
        try {
            String selectTableSQL = "SELECT uid, nombre, lat, lon, wifi, terraza, foto, " +
                    "AVG(valoracion), ROUND(SQRT(POWER((lat - ?) * 60 * 1852, 2) " +
                    "POWER((((lon - ?) * COS(lon * PI() / 180)) * 60 * 1852), 2))) AS distancia" +
                    "FROM bar JOIN partido p ON (uid = p.uid_bar) " +
                    "FULL OUTER JOIN valoracion v ON (uid = v.uid_bar) " +
                    "WHERE api=? GROUP BY uid ORDER BY distancia";
            stmt = conn.prepareStatement(selectTableSQL);
            stmt.setInt(1, api);
            stmt.setDouble(2, lat);
            stmt.setDouble(3, lon);
            ResultSet rs = stmt.executeQuery();
            while (rs.next())
                bares.add(new Bar(rs.getString(1), rs.getString(2), rs.getFloat(3), rs.getFloat(4),
                        rs.getBoolean(5), rs.getBoolean(6), rs.getBytes(7),
                        (rs.getString(8) != null) ? rs.getFloat(8) : -1, rs.getInt(9)));
            rs.close();
            stmt.close();
        } catch (SQLException se) {
            se.printStackTrace();
            System.out.println(se.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        return bares;
    }

    public Bar getWifiTerraceFromBar(String uid) {
        PreparedStatement stmt = null;
        Bar bar = new Bar();
        try {
            String selectTableSQL = "SELECT wifi, terraza FROM bar WHERE uid=?";
            stmt = conn.prepareStatement(selectTableSQL);
            stmt.setString(1, uid);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                bar.setWifi(rs.getBoolean(1));
                bar.setTerraza(rs.getBoolean(2));
            }
            rs.close();
            stmt.close();
        } catch (SQLException se) {
            se.printStackTrace();
            System.out.println(se.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        return bar;
    }

    public void updateWifi(String uid, Boolean wifi) {
        PreparedStatement stmt = null;
        try {
            String updateTableSQL = "UPDATE bar SET wifi=? WHERE uid=?";
            stmt = conn.prepareStatement(updateTableSQL);
            stmt.setBoolean(1, wifi);
            stmt.setString(2, uid);
            stmt.executeUpdate();
            stmt.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    public void updateLocation(String uid, double lat, double lon) {
        PreparedStatement stmt = null;
        try {
            String updateTableSQL = "UPDATE bar SET lat=?, lon=? WHERE uid=?";
            stmt = conn.prepareStatement(updateTableSQL);
            stmt.setDouble(1, lat);
            stmt.setDouble(2, lon);
            stmt.setString(3, uid);
            stmt.executeUpdate();
            stmt.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    public void updateTerrace(String uid, Boolean terrace) {
        PreparedStatement stmt = null;
        try {
            String updateTableSQL = "UPDATE bar SET terraza=? WHERE uid=?";
            stmt = conn.prepareStatement(updateTableSQL);
            stmt.setBoolean(1, terrace);
            stmt.setString(2, uid);
            stmt.executeUpdate();
            stmt.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    public void updatePhotoBar(String uid, byte[] bytea) {
        PreparedStatement stmt = null;
        try {
            String updateTableSQL = "UPDATE bar SET foto=? WHERE uid=?";
            stmt = conn.prepareStatement(updateTableSQL);
            stmt.setBytes(1, bytea);
            stmt.setString(2, uid);
            stmt.executeUpdate();
            stmt.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    /* VALORATION */

    public void createValoration(Valoration valoration) {
        PreparedStatement stmt = null;
        try {
            String insertTableSQL = "INSERT INTO valoracion VALUES(?,?,?,?)";
            stmt = conn.prepareStatement(insertTableSQL);
            stmt.setString(1, valoration.getUidUser());
            stmt.setString(2, valoration.getUidBar());
            stmt.setInt(3, Math.round(valoration.getPuntuation()));
            stmt.setString(4, valoration.getDescription());
            stmt.executeUpdate();
            stmt.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    public void updateValoration(Valoration valoration) {
        PreparedStatement stmt = null;
        try {
            String updateTableSQL = "UPDATE valoracion SET valoracion=?, comentario=? WHERE " +
                    "uid_usuario=? AND uid_bar=?";
            stmt = conn.prepareStatement(updateTableSQL);
            stmt.setInt(1, Math.round(valoration.getPuntuation()));
            stmt.setString(2, valoration.getDescription());
            stmt.setString(3, valoration.getUidUser());
            stmt.setString(4, valoration.getUidBar());
            stmt.executeUpdate();
            stmt.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    public Valoration getValoration(String uidUser, String uidBar) {
        Valoration valoration = null;
        PreparedStatement stmt = null;
        try {
            String selectTableSQL = "SELECT comentario, valoracion FROM valoracion WHERE " +
                    "uid_usuario = ? AND uid_bar = ?";
            stmt = conn.prepareStatement(selectTableSQL);
            stmt.setString(1, uidUser);
            stmt.setString(2, uidBar);
            ResultSet rs = stmt.executeQuery();
            while (rs.next())
                valoration = new Valoration(uidUser, uidBar, rs.getString(1), rs.getFloat(2));
            rs.close();
            stmt.close();
        } catch (SQLException se) {
            se.printStackTrace();
            System.out.println(se.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        return valoration;
    }

    public ArrayList<Valoration> getValorationsFromUser(String uid) {
        ArrayList<Valoration> valorations = new ArrayList<Valoration>();
        PreparedStatement stmt = null;
        try {
            String selectTableSQL = "SELECT uid_usuario, uid_bar, nombre, comentario, valoracion " +
                    "FROM valoracion JOIN bar ON (uid = uid_bar) WHERE uid_usuario = ?";
            stmt = conn.prepareStatement(selectTableSQL);
            stmt.setString(1, uid);
            ResultSet rs = stmt.executeQuery();
            while (rs.next())
                valorations.add(new Valoration(rs.getString(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getFloat(5)));
            rs.close();
            stmt.close();
        } catch (SQLException se) {
            se.printStackTrace();
            System.out.println(se.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        return valorations;
    }

    public ArrayList<Valoration> getValorationsFromBar(String uid) {
        ArrayList<Valoration> valorations = new ArrayList<Valoration>();
        PreparedStatement stmt = null;
        try {
            String selectTableSQL = "SELECT uid_usuario, uid_bar, nombre_completo, comentario, " +
                    "valoracion FROM valoracion JOIN usuario ON (uid = uid_usuario) WHERE " +
                    "uid_bar = ?";
            stmt = conn.prepareStatement(selectTableSQL);
            stmt.setString(1, uid);
            ResultSet rs = stmt.executeQuery();
            while (rs.next())
                valorations.add(new Valoration(rs.getString(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getFloat(5)));
            rs.close();
            stmt.close();
        } catch (SQLException se) {
            se.printStackTrace();
            System.out.println(se.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        return valorations;
    }

    /* MATCHES */

    public ArrayList<Match> getMatchesFromBar(String uid) {
        ArrayList<Match> matches = new ArrayList<Match>();
        PreparedStatement stmt = null;
        try {
            String selectTableSQL = "SELECT api, nombre_local, img_local, nombre_visitante, " +
                    "img_visitante, liga, img_liga, to_char(fecha, 'DD-MM-YYYY HH24:MI:SS') " +
                    "FROM partido WHERE uid_bar=? AND (fecha + interval '2 hour') >= now() " +
                    "ORDER BY fecha";
            stmt = conn.prepareStatement(selectTableSQL);
            stmt.setString(1, uid);
            ResultSet rs = stmt.executeQuery();
            while (rs.next())
                matches.add(new Match(uid, rs.getInt(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7),
                        rs.getString(8)));
            rs.close();
            stmt.close();
        } catch (SQLException se) {
            se.printStackTrace();
            System.out.println(se.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        return matches;
    }

    public ArrayList<Match> getMatchesFromTeam(String team) {
        ArrayList<Match> matches = new ArrayList<Match>();
        PreparedStatement stmt = null;
        try {
            String selectTableSQL = "SELECT DISTINCT api, nombre_local, img_local, nombre_visitante, " +
                    "img_visitante, liga, img_liga, to_char(fecha, 'DD-MM-YYYY HH24:MI:SS') AS fecha " +
                    "FROM partido WHERE (nombre_local=?  OR nombre_visitante=?) AND " +
                    "(fecha + interval '2 hour') >= now() ORDER BY fecha";
            stmt = conn.prepareStatement(selectTableSQL);
            stmt.setString(1, team);
            stmt.setString(2, team);
            ResultSet rs = stmt.executeQuery();
            while (rs.next())
                matches.add(new Match(rs.getInt(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7),
                        rs.getString(8)));
            rs.close();
            stmt.close();
        } catch (SQLException se) {
            se.printStackTrace();
            System.out.println(se.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        return matches;
    }

    public ArrayList<Match> getMatchesFromLeague(String league) {
        ArrayList<Match> matches = new ArrayList<Match>();
        PreparedStatement stmt = null;
        try {
            String selectTableSQL = "SELECT DISTINCT api, nombre_local, img_local, " +
                    "nombre_visitante, img_visitante, liga, img_liga, to_char(fecha, " +
                    "'DD-MM-YYYY HH24:MI:SS') AS fecha FROM partido WHERE liga=? " +
                    "AND (fecha + interval '2 hour') >= now() ORDER BY fecha;";
            stmt = conn.prepareStatement(selectTableSQL);
            stmt.setString(1, league);
            ResultSet rs = stmt.executeQuery();
            while (rs.next())
                matches.add(new Match(rs.getInt(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7),
                        rs.getString(8)));
            rs.close();
            stmt.close();
        } catch (SQLException se) {
            se.printStackTrace();
            System.out.println(se.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        return matches;
    }

    public void insertMatchFromBar(Match match, String uid) {
        PreparedStatement stmt = null;
        try {
            String insertTableSQL = "INSERT INTO partido VALUES(?,?,?,?,?,?,?,?,?)";
            stmt = conn.prepareStatement(insertTableSQL);
            stmt.setString(1, uid);
            stmt.setInt(2, match.getApi());
            stmt.setString(3, match.getLocal());
            stmt.setString(4, match.getLocalShield());
            stmt.setString(5, match.getVisitor());
            stmt.setString(6, match.getVisitorShield());
            stmt.setString(7, match.getLeague());
            stmt.setString(8, match.getLeagueShield());
            stmt.setTimestamp(9, match.getTimestamp());
            stmt.executeUpdate();
            stmt.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    public void deleteMatchFromBar(String uid, int api) {
        PreparedStatement stmt = null;
        try {
            String deleteTableSQL = "DELETE FROM partido WHERE uid_bar=? AND api=?";
            stmt = conn.prepareStatement(deleteTableSQL);
            stmt.setString(1, uid);
            stmt.setInt(2, api);
            stmt.executeQuery();
            stmt.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    /* LIGAS */

    public ArrayList<League> getLeagues() {
        ArrayList<League> leagues = new ArrayList<League>();
        PreparedStatement stmt = null;
        try {
            String selectTableSQL = "SELECT liga, img_liga FROM partido WHERE (fecha + interval " +
                    "'2 hour') >= now() GROUP BY liga, img_liga";
            stmt = conn.prepareStatement(selectTableSQL);
            ResultSet rs = stmt.executeQuery();
            while (rs.next())
                leagues.add(new League(rs.getString(1), rs.getString(2)));
            rs.close();
            stmt.close();
        } catch (SQLException se) {
            se.printStackTrace();
            System.out.println(se.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        return leagues;
    }

    /* FAVORITE BAR */

    public void createFavoriteBar(String uidUser, String uidBar) {
        PreparedStatement stmt = null;
        try {
            String insertTableSQL = "INSERT INTO favorito VALUES(?,?)";
            stmt = conn.prepareStatement(insertTableSQL);
            stmt.setString(1, uidUser);
            stmt.setString(2, uidBar);
            stmt.executeUpdate();
            stmt.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    public boolean userIsFavOfTheBar(String uidUser, String uidBar) {
        boolean isFavOfTheBar = false;
        PreparedStatement stmt = null;
        try {
            String selectTableSQL = "SELECT * FROM favorito WHERE uid_usuario=? AND uid_bar=?";
            stmt = conn.prepareStatement(selectTableSQL);
            stmt.setString(1, uidUser);
            stmt.setString(2, uidBar);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) isFavOfTheBar = true;
            rs.close();
            stmt.close();
        } catch (SQLException se) {
            se.printStackTrace();
            System.out.println(se.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        return isFavOfTheBar;
    }

    public ArrayList<Bar> getFavoriteBar(String uid) {
        ArrayList<Bar> bares = new ArrayList<Bar>();
        PreparedStatement stmt = null;
        try {
            String selectTableSQL = "SELECT uid, nombre, lat, lon, wifi, terraza, foto, " +
                    "AVG(valoracion) FROM favorito f JOIN bar ON (uid = f.uid_bar) " +
                    "FULL OUTER JOIN valoracion v ON (uid = v.uid_bar) " +
                    "WHERE f.uid_usuario = ? GROUP BY uid;";
            stmt = conn.prepareStatement(selectTableSQL);
            stmt.setString(1, uid);
            ResultSet rs = stmt.executeQuery();
            while (rs.next())
                bares.add(new Bar(rs.getString(1), rs.getString(2), rs.getFloat(3), rs.getFloat(4),
                        rs.getBoolean(5), rs.getBoolean(6), rs.getBytes(7),
                        (rs.getString(8) != null) ? rs.getFloat(8) : -1, -1));
            rs.close();
            stmt.close();
        } catch (SQLException se) {
            se.printStackTrace();
            System.out.println(se.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        return bares;
    }

    public ArrayList<Bar> getFavoriteBar(String uid, double lat, double lon) {
        ArrayList<Bar> bares = new ArrayList<Bar>();
        PreparedStatement stmt = null;
        try {
            String selectTableSQL = "SELECT uid, nombre, lat, lon, wifi, terraza, foto, " +
                    "AVG(valoracion), ROUND(SQRT(POWER((lat - ?) * 60 * 1852, 2) + POWER((((lon - ?) " +
                    "* COS(lat * PI() / 180)) * 60 * 1852), 2))) AS distancia " +
                    "FROM favorito f JOIN bar ON (uid = f.uid_bar) " +
                    "FULL OUTER JOIN valoracion v ON (uid = v.uid_bar) " +
                    "WHERE f.uid_usuario = ? GROUP BY uid ORDER BY distancia";
            stmt = conn.prepareStatement(selectTableSQL);
            stmt.setString(1, uid);
            stmt.setDouble(2, lat);
            stmt.setDouble(3, lon);
            ResultSet rs = stmt.executeQuery();
            while (rs.next())
                bares.add(new Bar(rs.getString(1), rs.getString(2), rs.getFloat(3), rs.getFloat(4),
                        rs.getBoolean(5), rs.getBoolean(6), rs.getBytes(7),
                        (rs.getString(8) != null) ? rs.getFloat(8) : -1, rs.getInt(9)));
            rs.close();
            stmt.close();
        } catch (SQLException se) {
            se.printStackTrace();
            System.out.println(se.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        return bares;
    }

    public void deleteFavoriteBar(String uidUser, String uidBar) {
        PreparedStatement stmt = null;
        try {
            String deleteTableSQL = "DELETE FROM favorito WHERE uid_usuario=? AND uid_bar=?";
            stmt = conn.prepareStatement(deleteTableSQL);
            stmt.setString(1, uidUser);
            stmt.setString(2, uidBar);
            stmt.executeQuery();
            stmt.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

}
