package sbr.m15.dam.local.sportbarradar.model;

import java.io.Serializable;

public class SavedLogin implements Serializable {
    private String uid;
    private String role;
    private String email;

    public SavedLogin(String uid, String role, String email) {
        this.uid = uid;
        this.role = role;
        this.email = email;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
