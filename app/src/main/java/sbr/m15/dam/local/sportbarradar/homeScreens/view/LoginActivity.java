package sbr.m15.dam.local.sportbarradar.homeScreens.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.OnClick;
import sbr.m15.dam.local.sportbarradar.R;
import sbr.m15.dam.local.sportbarradar.bases.BaseActivity;
import sbr.m15.dam.local.sportbarradar.dialogs.RecoveryPasswordDialog;
import sbr.m15.dam.local.sportbarradar.dialogs.RegisterOptionsDialog;
import sbr.m15.dam.local.sportbarradar.homeScreens.presenter.LoginPresenterImpl;
import sbr.m15.dam.local.sportbarradar.homeScreens.presenter.interfaces.LoginPresenter;
import sbr.m15.dam.local.sportbarradar.homeScreens.view.interfaces.LoginView;
import sbr.m15.dam.local.sportbarradar.model.Bar;
import sbr.m15.dam.local.sportbarradar.model.SavedLogin;
import sbr.m15.dam.local.sportbarradar.model.User;

public class LoginActivity extends BaseActivity implements LoginView,
        RecoveryPasswordDialog.RecoveryDialogListener,
        RegisterOptionsDialog.RegistrationDialogListener {

    @BindView(R.id.email)
    TextInputLayout email;
    @BindView(R.id.password)
    TextInputLayout password;
    @BindView(R.id.editTextEmail)
    TextInputEditText editTextEmail;
    @BindView(R.id.editTextPassword)
    TextInputEditText editTextPassword;
    @BindView(R.id.btnRegister)
    Button btnRegister;
    @BindView(R.id.btnRecoveryPassword)
    TextView btnRecoveryPassword;

    private LoginPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        presenter = new LoginPresenterImpl(this);
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            String msg = bundle.getString("msgRegister");
            if (msg != null) {
                showMessage(msg);
            }
        }
    }

    private void showMessage(String msg) {
        Snackbar.make(password, msg, Snackbar.LENGTH_LONG)
                .show();
    }

    //on click del logo solo para pruebas
    @OnClick({R.id.btnLogin, R.id.btnRegister, R.id.btnRecoveryPassword})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnLogin:
                presenter.validateCredentials(editTextEmail.getText().toString(), editTextPassword.getText().toString());
                break;
            case R.id.btnRegister:
                register();
                break;
            case R.id.btnRecoveryPassword:
                forgotPassword();
                break;
        }
    }

    @Override
    public void setErrorMail() {
        email.setError(getString(R.string.errorEmailNotExist));
    }

    @Override
    public void setErrorEmptyMail() {
        email.setError(getString(R.string.errorEmptyField));
    }

    @Override
    public void setErrorNotValidMail() {
        email.setError(getString(R.string.errorEmailNotValid));
    }

    @Override
    public void setErrorPassword() {
        password.setError(getString(R.string.errorIncorrectPassword));
    }

    @Override
    public void setErrorEmptyPassword() {
        password.setError(getString(R.string.errorEmptyField));
    }

    @Override
    public void register() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        RegisterOptionsDialog newFragment = new RegisterOptionsDialog();
        newFragment.setCancelable(false);
        newFragment.show(fragmentManager, "register");
    }

    @Override
    public void forgotPassword() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        RecoveryPasswordDialog newFragment = new RecoveryPasswordDialog();
        newFragment.setCancelable(false);
        newFragment.show(fragmentManager, "dialog");
    }

    @Override
    public void redirect(Class home) {
        redirectNotFinish(home);
        finish();
    }

    public void redirectNotFinish(Class home) {
        Intent i = new Intent(this, home);
        startActivity(i);
    }

    @Override
    public void setErrorNotConnection() {
        hideProgress();
        Snackbar.make(password, R.string.errorNotConnection, Snackbar.LENGTH_LONG).show();
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void cleanErrors() {
        password.setError("");
        email.setError("");
    }

    @Override
    public void setErrorRecoveryMail() {
        Snackbar.make(password, R.string.errorEmailNotExist, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onRecoveryMailSuccessful() {
        Snackbar.make(password, R.string.ntfReciveEmail, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onRecoveryNotConnection() {
        Snackbar.make(password, R.string.errorNotConnection, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void saveSavedData(String uid, String role, String email) {
        SavedLogin user = new SavedLogin(uid, role, email);
        saveFileSavedLogin(user);
    }

    @Override
    public void saveUserData(User user) {
        saveFileUserData(user);
    }

    @Override
    public void saveBarData(Bar bar) {
        saveFileBarData(bar);
    }

    @Override
    public void onRecoveryPositiveClick(DialogFragment dialog, String mail) {
        presenter.recoveryAccept(mail);
        dialog.dismiss();
    }

    @Override
    public void onUserRoleSelected() {
        redirectNotFinish(RegisterUserActivity.class);
    }

    @Override
    public void onBarRoleSelected() {
        redirectNotFinish(RegisterBarActivity.class);
    }

    @Override
    public void onRestart() {
        super.onRestart();
        editTextEmail.setText("");
        editTextPassword.setText("");
    }
}
