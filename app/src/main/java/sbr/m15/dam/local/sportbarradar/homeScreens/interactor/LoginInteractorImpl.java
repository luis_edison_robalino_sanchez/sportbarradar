package sbr.m15.dam.local.sportbarradar.homeScreens.interactor;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseNetworkException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.google.firebase.auth.FirebaseUser;

import sbr.m15.dam.local.sportbarradar.barScreens.BarMainActivity;
import sbr.m15.dam.local.sportbarradar.db.Connexion;
import sbr.m15.dam.local.sportbarradar.homeScreens.interactor.interfaces.LoginInteractor;
import sbr.m15.dam.local.sportbarradar.userScreens.UserMainActivity;
import sbr.m15.dam.local.sportbarradar.utils.FormUtils;

public class LoginInteractorImpl implements LoginInteractor {
    private FirebaseAuth mAuth;
    private OnLoginFinishedListener listener;

    public LoginInteractorImpl(OnLoginFinishedListener listener) {
        mAuth = FirebaseAuth.getInstance();
        this.listener = listener;
    }

    @Override
    public void requestResetPassword(String email) {
        mAuth.sendPasswordResetEmail(email)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            listener.onRecoverySuccessful();
                        } else {
                            try {
                                throw task.getException();
                            } catch (FirebaseAuthInvalidUserException e) {
                                e.printStackTrace();
                                listener.onRecoveryMailError();
                            } catch (FirebaseNetworkException e) {
                                e.printStackTrace();
                                listener.onRecoveryNotConnection();
                            } catch (Exception e) {
                                e.printStackTrace();
                                Log.e("ERROR RECOVERY:", e.getMessage());
                            }
                        }
                    }
                });
    }

    @Override
    public void login(String email, String password) {
        boolean isCorrect = true;

        if (!FormUtils.isEmailValid(email)) {
            listener.onMailNotValid();
            isCorrect = false;
        }

        if (!FormUtils.isNotEmpty(email)) {
            listener.onMailEmpty();
            isCorrect = false;
        }

        if (!FormUtils.isNotEmpty(password)) {
            listener.onPasswordEmpty();
            isCorrect = false;
        }

        if (isCorrect) {
            mAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                Intent intent;
                                FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
                                Connexion conn = new Connexion();
                                try {
                                    conn.conectar();
                                    if (conn.getUsuario(firebaseUser.getUid()) != null) {
                                        listener.saveSavedData(firebaseUser.getUid(), "user", firebaseUser.getEmail());
                                        listener.saveUserData(conn.getUsuario(firebaseUser.getUid()));
                                        listener.onSuccess(UserMainActivity.class);
                                    } else if (conn.getBar(firebaseUser.getUid()) != null) {
                                        listener.saveSavedData(firebaseUser.getUid(), "bar", firebaseUser.getEmail());
                                        listener.saveBarData(conn.getBar(firebaseUser.getUid()));
                                        listener.onSuccess(BarMainActivity.class);
                                    } else {
                                        listener.onNotConnection();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Log.e("ERROR LOGIN:", e.getMessage());
                                } finally {
                                    if (conn.isConnected()) conn.desconectar();
                                }
                            } else {
                                try {
                                    throw task.getException();
                                } catch (FirebaseAuthInvalidUserException e) {
                                    listener.onMailError();
                                } catch (FirebaseAuthInvalidCredentialsException e) {
                                    listener.onPasswordError();
                                } catch (FirebaseNetworkException e) {
                                    listener.onNotConnection();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Log.e("ERROR LOGIN:", e.getMessage());
                                }
                            }
                        }
                    });
        }
    }

    //no se donde se llamaba a este metodo
    private void signOut() {
        mAuth.signOut();
    }
}
