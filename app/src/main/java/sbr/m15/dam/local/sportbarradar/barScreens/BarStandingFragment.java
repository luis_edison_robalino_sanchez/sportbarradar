package sbr.m15.dam.local.sportbarradar.barScreens;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.ArrayList;

import butterknife.BindView;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.HttpException;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import sbr.m15.dam.local.sportbarradar.R;
import sbr.m15.dam.local.sportbarradar.adapter.MatchesAdapter;
import sbr.m15.dam.local.sportbarradar.adapter.MatchesLeagueAdapter;
import sbr.m15.dam.local.sportbarradar.api.ResultadosFutbol;
import sbr.m15.dam.local.sportbarradar.bases.BaseFragment;
import sbr.m15.dam.local.sportbarradar.db.Connexion;
import sbr.m15.dam.local.sportbarradar.model.League;
import sbr.m15.dam.local.sportbarradar.model.Ligas;
import sbr.m15.dam.local.sportbarradar.model.Match;
import sbr.m15.dam.local.sportbarradar.model.MatchDay;

public class BarStandingFragment extends BaseFragment implements MatchesAdapter.MatchesItemClickListener, MatchesLeagueAdapter.MatchesLeagueItemClickListener {

    @BindView(R.id.pbHeaderProgress)
    ProgressBar pbHeaderProgress;
    @BindView(R.id.listadoLigas)
    RecyclerView listadoLigas;

    private ArrayList<Match> partidos;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private ResultadosFutbol apiService;
    private ArrayList<League> ligas = new ArrayList<>();

    private Subscription subscription;
    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(ResultadosFutbol.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io()))
            .build();

    public BarStandingFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list_leagues, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pbHeaderProgress.setVisibility(View.VISIBLE);
        partidos = new ArrayList<>();
        mAdapter = (getArguments() == null) ? new MatchesLeagueAdapter(partidos,this):new MatchesAdapter(partidos,this);
        mLayoutManager = new LinearLayoutManager(getContext());
        listadoLigas.setLayoutManager(mLayoutManager);
        listadoLigas.setAdapter(mAdapter);
        apiService = retrofit.create(ResultadosFutbol.class);
        if (getArguments() != null) {
            League l = (League)getArguments().getSerializable("liga");
            addMatchs(l);
        }else{
            addMatchesFromLeagues("es");
        }
    }

    private void addMatchs(final League liga) {
        Observable<MatchDay> call = apiService.getLeagueNextMatches(liga.getId(), liga.getRound());
        subscription = call
                .subscribeOn(Schedulers.io()) // optional if you do not wish to override the default behavior
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Subscriber<MatchDay>() {
                    @Override
                    public void onCompleted() {
                        if (partidos.size() == 0) {
                            addMatchsNextRound(liga);
                        }
                        mAdapter.notifyDataSetChanged();
                        if (pbHeaderProgress != null)
                            pbHeaderProgress.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(Throwable e) {
                        // cast to retrofit.HttpException to get the response code
                        if (e instanceof HttpException) {
                            HttpException response = (HttpException) e;
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onNext(MatchDay ta) {
                        for (Match m : ta.getMatchs())
                            if (m.getResult().equalsIgnoreCase("x-x")) {
                                m.setLeagueShield(liga.getLogoUrl());
                                partidos.add(m);
                            }
                    }
                });
    }

    private void addMatchsNextRound(League liga) {
        liga.setRound(liga.getNextRound());
        addMatchs(liga);
    }

    private void addMatchesFromLeagues(String es) {
        Observable<Ligas> call = apiService.getLigas(es);
        subscription = call
                .subscribeOn(Schedulers.io()) // optional if you do not wish to override the default behavior
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Subscriber<Ligas>() {
                    @Override
                    public void onCompleted() {
                        for(League l : ligas)
                            addMatchs(l);
                    }

                    @Override
                    public void onError(Throwable e) {
                        // cast to retrofit.HttpException to get the response code
                        if (e instanceof HttpException) {
                            HttpException response = (HttpException) e;
                            e.printStackTrace();
                        }
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(Ligas ta) {

                        for (League l : ta.getCategory().getSpain().getLigas())
                            if (l.getRound() != l.getTotalRounds())
                                ligas.add(l);

                        for (League c : ta.getCategory().getSpain().getCompeticiones())
                            if (c.getRound() != c.getTotalRounds())
                                ligas.add(c);
                    }
                });
    }

    @Override
    public void itemClicked(Match m) {
        showConfirmationAddMatchDialog(m);
    }

    private void showConfirmationAddMatchDialog(final Match m) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage(R.string.dialogAddMatchBar)
                .setPositiveButton(R.string.btnAccept, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        addMatch(m);
                    }
                })
                .setNegativeButton(R.string.btnCancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void addMatch(Match m) {
        if (getArguments() != null) {
            League l = (League)getArguments().getSerializable("liga");
            m.setLeagueShield(l.getLogoUrl());
        }
        Connexion conn = new Connexion();
        try {
            conn.conectar();
            conn.insertMatchFromBar(m, getUidFromFile());
            Snackbar.make(getView(), R.string.msgAddMatch, Snackbar.LENGTH_SHORT).show();
        } catch (Exception e) {
            Log.e("SQL", e.getMessage());
            Snackbar.make(getView(), R.string.errorNotConnection, Snackbar.LENGTH_SHORT).show();
            e.printStackTrace();
        } finally {
            if (conn.isConnected()) conn.desconectar();
        }
    }

    public static BarStandingFragment newInstance(League l) {
        BarStandingFragment fragment = new BarStandingFragment();
        Bundle args = new Bundle();
        args.putSerializable("liga", l);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (null != subscription)
            subscription.unsubscribe();
    }
}
