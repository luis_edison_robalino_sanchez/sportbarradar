package sbr.m15.dam.local.sportbarradar.bases;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import sbr.m15.dam.local.sportbarradar.services.LocationService;

public class LocationBaseFragment extends BaseFragment {

    private BroadcastReceiver broadcastReceiver;
    private Intent locationIntent;

    protected double latitude;
    protected double longitude;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        locationIntent = new Intent(getContext(), LocationService.class);
        getActivity().startService(locationIntent);
        if (broadcastReceiver == null) {
            broadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    Bundle bundle = getActivity().getIntent().getExtras();
                    latitude = bundle.getDouble("latitude");
                    longitude = bundle.getDouble("longitude");
                }
            };
        }
        getActivity().registerReceiver(broadcastReceiver, new IntentFilter("locationUpdate"));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getActivity().stopService(locationIntent);
    }

}
