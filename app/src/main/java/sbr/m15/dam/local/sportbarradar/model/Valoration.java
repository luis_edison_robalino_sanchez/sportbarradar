package sbr.m15.dam.local.sportbarradar.model;

public class Valoration {

    private String uidUser;
    private String uidBar;
    private String name;
    private String description;
    private Float puntuation;

    public Valoration(String uidUser, String uidBar, String name, String description, Float puntuation) {
        this(uidUser, uidBar, description, puntuation);
        this.name = name;
    }

    public Valoration(String uidUser, String uidBar, String description, Float puntuation) {
        this.uidUser = uidUser;
        this.uidBar = uidBar;
        this.description = description;
        this.puntuation = puntuation;
    }

    public Valoration(String name, String description, Float puntuation) {
        this.name = name;
        this.description = description;
        this.puntuation = puntuation;
    }

    public String getUidUser() {
        return uidUser;
    }

    public void setUidUser(String uidUser) {
        this.uidUser = uidUser;
    }

    public String getUidBar() {
        return uidBar;
    }

    public void setUidBar(String uidBar) {
        this.uidBar = uidBar;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getPuntuation() {
        return puntuation;
    }

    public void setPuntuation(Float puntuation) {
        this.puntuation = puntuation;
    }
}
