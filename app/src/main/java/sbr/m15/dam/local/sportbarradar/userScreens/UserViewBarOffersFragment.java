package sbr.m15.dam.local.sportbarradar.userScreens;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import sbr.m15.dam.local.sportbarradar.R;
import sbr.m15.dam.local.sportbarradar.bases.BaseFragment;

public class UserViewBarOffersFragment extends BaseFragment {


    public UserViewBarOffersFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_bar_offers, container, false);
    }

    public static UserViewBarOffersFragment newInstance() {
        UserViewBarOffersFragment fragment = new UserViewBarOffersFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }
}
