package sbr.m15.dam.local.sportbarradar.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Category {

    @SerializedName("spain")
    @Expose
    private Spain spain;

    public Spain getSpain() {
        return spain;
    }

    public void setSpain(Spain spain) {
        this.spain = spain;
    }

}