package sbr.m15.dam.local.sportbarradar.barScreens;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;

import com.ncapdevi.fragnav.FragNavController;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabReselectListener;
import com.roughike.bottombar.OnTabSelectListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import sbr.m15.dam.local.sportbarradar.R;
import sbr.m15.dam.local.sportbarradar.bases.BaseActivity;

public class BarMainActivity extends BaseActivity {

    @BindView(R.id.bottomBar)
    BottomBar bottomBar;
    private FragNavController fragNavController;

    private final int INDEX_PARTIDOS = FragNavController.TAB1;
    private final int INDEX_PERFIL = FragNavController.TAB2;
    private final int INDEX_OFERTAS = FragNavController.TAB3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bar_main);

        List<Fragment> fragments = new ArrayList<>(3);

        fragments.add(BarTabsPartidosFragment.newInstance());
        fragments.add(BarProfileFragment.newInstance());
        fragments.add(BarOffersFragment.newInstance());
        fragNavController = new FragNavController(savedInstanceState, getSupportFragmentManager(), R.id.contenedorBar, fragments, INDEX_PERFIL);
        bottomBar.selectTabAtPosition(INDEX_PERFIL);
        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {
                switch (tabId) {
                    case R.id.tab_equipos:
                        fragNavController.switchTab(INDEX_PARTIDOS);
                        break;
                    case R.id.tab_perfil:
                        fragNavController.switchTab(INDEX_PERFIL);
                        break;
                    case R.id.tab_ofertas:
                        fragNavController.switchTab(INDEX_OFERTAS);
                        break;
                }

            }
        });
        bottomBar.setOnTabReselectListener(new OnTabReselectListener() {
            @Override
            public void onTabReSelected(@IdRes int tabId) {
                fragNavController.clearStack();
            }
        });
    }

    @Override
    public void onBackPressed() {
        boolean salir = true;
        Fragment f = getSupportFragmentManager().findFragmentByTag("partidosDeXLiga");
        if (f != null) {
            getSupportFragmentManager().beginTransaction().remove(f).commit();
            salir = false;
        }

        if (salir) {
            if (fragNavController.getCurrentStack().size() > 1) {
                fragNavController.popFragment();
            } else {
                super.onBackPressed();
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        bottomBar.onSaveInstanceState();
        if (fragNavController != null) {
            fragNavController.onSaveInstanceState(outState);
        }
    }
}
