package sbr.m15.dam.local.sportbarradar.homeScreens.interactor;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseNetworkException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.ProviderQueryResult;

import sbr.m15.dam.local.sportbarradar.db.Connexion;
import sbr.m15.dam.local.sportbarradar.homeScreens.interactor.interfaces.RegisterUserInteractor;
import sbr.m15.dam.local.sportbarradar.model.User;
import sbr.m15.dam.local.sportbarradar.utils.FormUtils;

public class RegisterUserInteractorImpl implements RegisterUserInteractor {
    private FirebaseAuth mAuth;
    private OnRegisterUserFinishedListener listener;
    private String password;
    private User user;

    public RegisterUserInteractorImpl(OnRegisterUserFinishedListener listener) {
        mAuth = FirebaseAuth.getInstance();
        this.listener = listener;
    }

    @Override
    public void validateRegister(User user) {
        boolean isCorrect = true;
        this.user = user;

        if (!FormUtils.isNotEmpty(user.getNombre())) {
            listener.onErrorEmptyName();
            isCorrect = false;
        }

        if (!FormUtils.isEmailValid(user.getEmail())) {
            listener.onErrorNotValidMail();
            isCorrect = false;
        }

        if (!FormUtils.isNotEmpty(user.getEmail())) {
            listener.onErrorEmptyMail();
            isCorrect = false;
        }

        if (user.getPassword().length() < 6) {
            listener.onErrorShortPassword();
            isCorrect = false;
        }

        if (!FormUtils.isNotEmpty(user.getPassword())) {
            listener.onErrorEmptyPassword();
            isCorrect = false;
        }

        if (!FormUtils.isNotEmpty(user.getRepeatPassword())) {
            listener.onErrorEmptyRepeatPassword();
            isCorrect = false;
        }

        if (FormUtils.isNotEmpty(user.getPassword()) && FormUtils.isNotEmpty(user.getRepeatPassword())
                && !user.getPassword().equals(user.getRepeatPassword())) {
            listener.onErrorPasswordNotEqual();
            isCorrect = false;
        }

        if (0 == user.getEquipoFavorito().getId()) {
            listener.onErrorNotSelectedTeam();
            isCorrect = false;
        }

        if (isCorrect) {
            mAuth.fetchProvidersForEmail(user.getEmail())
                    .addOnCompleteListener(new OnCompleteListener<ProviderQueryResult>() {
                        @Override
                        public void onComplete(@NonNull Task<ProviderQueryResult> task) {
                            if (task.isSuccessful()) {
                                if (task.getResult().getProviders().size() == 0) {
                                    registerBar();
                                } else {
                                    listener.onErrorMail();
                                }
                            } else {
                                try {
                                    throw task.getException();
                                } catch (FirebaseNetworkException e) {
                                    listener.onErrorNotConection();
                                } catch (Exception e) {
                                    listener.onErrorNotConection();
                                }
                            }
                        }
                    });
        }
    }

    private void registerBar() {
        mAuth.createUserWithEmailAndPassword(user.getEmail(), user.getPassword())
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
                            Connexion conn = new Connexion();
                            try {
                                conn.conectar();
                                if (!conn.isConnected()) throw new Exception();
                                User u2 = new User(firebaseUser.getUid(), user.getNombre());
                                u2.setEquipoFavorito(user.getEquipoFavorito());
                                conn.createUsuario(u2);
                                listener.onSuccess();
                            } catch (Exception ex) {
                                FormUtils.deleteUserFirebase(firebaseUser);
                                listener.onErrorNotConection();
                            } finally {
                                if (conn.isConnected()) conn.desconectar();
                            }
                        } else {
                            try {
                                throw task.getException();
                            } catch (FirebaseNetworkException e) {
                                listener.onErrorNotConection();
                            } catch (Exception e) {
                                listener.onErrorNotConection();
                            }
                        }
                    }
                });
    }


}
