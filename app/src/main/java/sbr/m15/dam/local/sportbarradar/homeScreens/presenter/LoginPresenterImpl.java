package sbr.m15.dam.local.sportbarradar.homeScreens.presenter;

import sbr.m15.dam.local.sportbarradar.homeScreens.interactor.LoginInteractorImpl;
import sbr.m15.dam.local.sportbarradar.homeScreens.interactor.interfaces.LoginInteractor;
import sbr.m15.dam.local.sportbarradar.homeScreens.presenter.interfaces.LoginPresenter;
import sbr.m15.dam.local.sportbarradar.homeScreens.view.interfaces.LoginView;
import sbr.m15.dam.local.sportbarradar.model.Bar;
import sbr.m15.dam.local.sportbarradar.model.User;

public class LoginPresenterImpl implements LoginPresenter, LoginInteractor.OnLoginFinishedListener {
    private LoginView vista;
    private LoginInteractor loginInteractor;


    public LoginPresenterImpl(LoginView vista) {
        this.vista = vista;
        this.loginInteractor = new LoginInteractorImpl(this);
    }


    @Override
    public void validateCredentials(String username, String password) {
        if (vista != null) {
            vista.cleanErrors();
            vista.showProgress();
        }
        loginInteractor.login(username, password);
    }

    @Override
    public void onDestroy() {
        vista = null;
    }

    @Override
    public void recoveryAccept(String email) {
        loginInteractor.requestResetPassword(email);
    }

    @Override
    public void onMailError() {
        if (vista != null) {
            vista.hideProgress();
            vista.setErrorMail();
        }
    }

    @Override
    public void onMailNotValid() {
        if (vista != null) {
            vista.hideProgress();
            vista.setErrorNotValidMail();
        }
    }

    @Override
    public void onMailEmpty() {
        if (vista != null) {
            vista.hideProgress();
            vista.setErrorEmptyMail();
        }
    }

    @Override
    public void onPasswordEmpty() {
        if (vista != null) {
            vista.hideProgress();
            vista.setErrorEmptyPassword();
        }
    }

    @Override
    public void onPasswordError() {
        if (vista != null) {
            vista.hideProgress();
            vista.setErrorPassword();
        }
    }

    @Override
    public void onSuccess(Class home) {
        if (vista != null) {
            vista.hideProgress();
            vista.redirect(home);
        }
    }

    @Override
    public void onNotConnection() {
        vista.setErrorNotConnection();
    }


    @Override
    public void onRecoverySuccessful() {
        vista.onRecoveryMailSuccessful();
    }

    @Override
    public void onRecoveryMailError() {
        if (vista != null) {
            vista.setErrorRecoveryMail();
        }
    }

    @Override
    public void onRecoveryNotConnection() {
        vista.onRecoveryNotConnection();
    }

    @Override
    public void saveSavedData(String uid, String role, String email) {
        vista.saveSavedData(uid, role, email);
    }

    @Override
    public void saveUserData(User user) {
        vista.saveUserData(user);
    }

    @Override
    public void saveBarData(Bar bar) {
        vista.saveBarData(bar);
    }
}
