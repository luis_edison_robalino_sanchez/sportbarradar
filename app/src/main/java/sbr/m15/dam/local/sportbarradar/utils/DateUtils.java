package sbr.m15.dam.local.sportbarradar.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {

    public static String formatDate(String date) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
            Date parsedDate = dateFormat.parse(date);
            dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            return dateFormat.format(parsedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String formatDateEsp(String date) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            Date parsedDate = dateFormat.parse(date);
            dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            return dateFormat.format(parsedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }
}
