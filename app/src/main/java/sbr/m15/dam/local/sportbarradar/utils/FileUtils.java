package sbr.m15.dam.local.sportbarradar.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import sbr.m15.dam.local.sportbarradar.model.Bar;
import sbr.m15.dam.local.sportbarradar.model.SavedLogin;
import sbr.m15.dam.local.sportbarradar.model.Team;
import sbr.m15.dam.local.sportbarradar.model.User;

public class FileUtils {

    public static SavedLogin getSavedLoginFromFile(String path) {
        File f = new File(path);
        FileInputStream filein = null;
        try {
            filein = new FileInputStream(f);
            ObjectInputStream dataIS = new ObjectInputStream(filein);
            return (SavedLogin) dataIS.readObject();
        } catch (Exception e) {
            return new SavedLogin("2121","dsddd","dsda");
        }
    }

    public static User getUserFromFile(String path) {
        File f = new File(path);
        FileInputStream filein = null;
        try {
            filein = new FileInputStream(f);
            ObjectInputStream dataIS = new ObjectInputStream(filein);
            return (User) dataIS.readObject();
        } catch (Exception e) {
            return null;
        }
    }

    public static Bar getBarFromFile(String path) {
        File f = new File(path);
        FileInputStream filein = null;
        try {
            filein = new FileInputStream(f);
            ObjectInputStream dataIS = new ObjectInputStream(filein);
            return (Bar) dataIS.readObject();
        } catch (Exception e) {
            return null;
        }
    }

    public static String getUidFromFile(String path) {
        return getSavedLoginFromFile(path).getUid();
    }

    public static String getEmailFromFile(String path) {
        return getSavedLoginFromFile(path).getEmail();
    }

    public static void saveFileSavedLogin(String path, SavedLogin savedLogin) {
        File f = new File(path);
        FileOutputStream fileout = null;
        try {
            fileout = new FileOutputStream(f);
            ObjectOutputStream dataOS = new ObjectOutputStream(fileout);
            dataOS.writeObject(savedLogin);
            dataOS.close();
            fileout.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void saveFileUserData(String path, User user) {
        File f = new File(path);
        FileOutputStream fileout = null;
        try {
            fileout = new FileOutputStream(f);
            ObjectOutputStream dataOS = new ObjectOutputStream(fileout);
            dataOS.writeObject(user);
            dataOS.close();
            fileout.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void saveFileBarData(String path, Bar bar) {
        File f = new File(path);
        FileOutputStream fileout = null;
        try {
            fileout = new FileOutputStream(f);
            ObjectOutputStream dataOS = new ObjectOutputStream(fileout);
            dataOS.writeObject(bar);
            dataOS.close();
            fileout.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void deleteFile(String path) {
        File f = new File(path);
        f.delete();
    }

    public static void changeFavTeam(String path, Team team) {
        User user = getUserFromFile(path);
        user.setEquipoFavorito(team);
        saveFileUserData(path, user);
    }

    public static Team getFavTeam(String path) {
        return getUserFromFile(path).getEquipoFavorito();
    }

}
