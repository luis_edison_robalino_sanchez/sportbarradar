package sbr.m15.dam.local.sportbarradar.barScreens;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import sbr.m15.dam.local.sportbarradar.R;
import sbr.m15.dam.local.sportbarradar.adapter.ValorationsAdapter;
import sbr.m15.dam.local.sportbarradar.bases.ProfileBaseFragment;
import sbr.m15.dam.local.sportbarradar.db.Connexion;
import sbr.m15.dam.local.sportbarradar.model.Bar;
import sbr.m15.dam.local.sportbarradar.model.Valoration;

import static java.lang.Thread.sleep;

public class BarProfileFragment extends ProfileBaseFragment {

    private static final int PLACE_PICKER_REQUEST = 1;

    @BindView(R.id.txtUsername)
    TextView txtUsername;
    @BindView(R.id.listValorations)
    RecyclerView listValorations;
    @BindView(R.id.wifi)
    ImageView wifi;
    @BindView(R.id.terrace)
    ImageView terrace;

    private ValorationsAdapter adapter;
    private ArrayList<Valoration> valorations;
    private Bar barData;

    private PlacePicker.IntentBuilder builder;

    public BarProfileFragment() {
    }

    public static BarProfileFragment newInstance() {
        BarProfileFragment fragment = new BarProfileFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        barData = getBarFromFile();
        txtUsername.setText(barData.getNombre());
        loadWifiTerrace();
        showValorationsList();
    }

    private void showValorationsList() {
        valorations = new ArrayList<Valoration>();
        Connexion conn = new Connexion();
        try {
            conn.conectar();
            valorations = conn.getValorationsFromBar(getUidFromFile());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conn.desconectar();
        }
        adapter = new ValorationsAdapter(valorations);
        listValorations.setAdapter(adapter);
        listValorations.setLayoutManager(
                new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
    }

    @OnClick({R.id.menuConfig, R.id.imgProfile, R.id.wifi, R.id.terrace})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.menuConfig:
                showMenu(view);
                break;
            case R.id.imgProfile:
                changePhotoProfileDialog();
                break;
        }
    }

    private void loadWifiTerrace() {
        wifi.setVisibility(View.VISIBLE);
        terrace.setVisibility(View.VISIBLE);
        Connexion conn = new Connexion();
        try {
            conn.conectar();
            Bar b = conn.getWifiTerraceFromBar(barData.getUid());
            barData.setWifi(b.getWifi());
            barData.setTerraza(b.getTerraza());
            if (!barData.getWifi()) wifi.setColorFilter(
                    ContextCompat.getColor(getContext(), ICON_DESACTIVATE));
            if (!barData.getTerraza()) terrace.setColorFilter(
                    ContextCompat.getColor(getContext(), ICON_DESACTIVATE));
        } catch (Exception e) {
            Snackbar.make(getView(), R.string.errorNotConnection, Snackbar.LENGTH_SHORT).show();
            e.printStackTrace();
        } finally {
            conn.desconectar();
        }
    }

    private void showMenu(View view) {
        PopupMenu popup = new PopupMenu(getContext(), view);
        popup.getMenuInflater().inflate(R.menu.menu_profile_bar, popup.getMenu());
        changeTitleMenu(popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menuChangeLocation:
                        changeLocation();
                        break;
                    case R.id.menuChangeWifi:
                        changeStateWifi();
                        break;
                    case R.id.menuChangeTerrace:
                        changeStateTerrace();
                        break;
                    case R.id.menuChangePassword:
                        changePasswordDialog();
                        break;
                    case R.id.menuLogout:
                        logout();
                        break;
                }
                return true;
            }
        });
        popup.show();
    }

    private void changeLocation() {
        if (builder == null) {
            builder = new PlacePicker.IntentBuilder();
            try {
                startActivityForResult(builder.build(getActivity()), PLACE_PICKER_REQUEST);
            } catch (GooglePlayServicesRepairableException e) {
                Snackbar.make(getView(), R.string.errorGoogleServices, Snackbar.LENGTH_LONG)
                        .show();
            } catch (GooglePlayServicesNotAvailableException e) {
                Snackbar.make(getView(), R.string.errorGoogleServices, Snackbar.LENGTH_LONG)
                        .show();
            }
        }
    }

    private void changeTitleMenu(Menu menu) {
        menu.getItem(1).setTitle((barData.getWifi()) ?
                R.string.menuWifiDesactivate : R.string.menuWifiActivate);
        menu.getItem(2).setTitle((barData.getTerraza()) ?
                R.string.menuTerraceDesactivate : R.string.menuTerraceActivate);
    }

    private void changeStateWifi() {
        Connexion conn = new Connexion();
        try {
            conn.conectar();
            conn.updateWifi(barData.getUid(), !barData.getWifi());
            barData.setWifi(!barData.getWifi());
            wifi.setColorFilter((barData.getWifi()) ?
                    ContextCompat.getColor(getContext(), ICON_ACTIVATE) :
                    ContextCompat.getColor(getContext(), ICON_DESACTIVATE));
        } catch (Exception e) {
            try { //Para que el snackbar no se muestre con diferente tamaño
                sleep(2000);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
            Snackbar.make(getView(), R.string.errorNotConnection, Snackbar.LENGTH_SHORT).show();
            e.printStackTrace();
        } finally {
            conn.desconectar();
        }
    }

    private void changeStateTerrace() {
        Connexion conn = new Connexion();
        try {
            conn.conectar();
            conn.updateTerrace(barData.getUid(), !barData.getTerraza());
            barData.setTerraza(!barData.getTerraza());
            terrace.setColorFilter((barData.getTerraza()) ?
                    ContextCompat.getColor(getContext(), ICON_ACTIVATE) :
                    ContextCompat.getColor(getContext(), ICON_DESACTIVATE));
        } catch (Exception e) {
            Snackbar.make(getView(), R.string.errorNotConnection, Snackbar.LENGTH_SHORT).show();
            e.printStackTrace();
        } finally {
            conn.desconectar();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place p = PlacePicker.getPlace(data, getContext());
                LatLng coordenadas = p.getLatLng();
                Connexion conn = new Connexion();
                try {
                    conn.conectar();
                    conn.updateLocation(getUidFromFile(), coordenadas.latitude, coordenadas.longitude);
                    Snackbar.make(getView(), R.string.msgChangedLocation, Snackbar.LENGTH_SHORT).show();
                } catch (Exception e) {
                    Snackbar.make(getView(), R.string.errorNotConnection, Snackbar.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
            builder = null;
        }
    }

}
