package sbr.m15.dam.local.sportbarradar.adapter;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import sbr.m15.dam.local.sportbarradar.R;
import sbr.m15.dam.local.sportbarradar.model.Bar;
import sbr.m15.dam.local.sportbarradar.viewHolder.BaresViewHolder;

public class BaresAdapter extends RecyclerView.Adapter<BaresViewHolder> {

    ArrayList<Bar> bares;
    Context ctx;
    BarItemClickListener listener;

    public static interface BarItemClickListener {
        void itemClicked(Bar b);
    }

    public BaresAdapter(ArrayList<Bar> bares, Context c, BarItemClickListener l) {
        this.bares = bares;
        ctx = c;
        listener = l;
    }

    @Override
    public BaresViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_bares, parent, false);
        return new BaresViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(BaresViewHolder holder, final int position) {
        holder.bindBar(bares.get(position));
        holder.cardBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.itemClicked(bares.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return bares.size();
    }
}
