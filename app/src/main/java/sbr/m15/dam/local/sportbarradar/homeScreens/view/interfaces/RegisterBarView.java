package sbr.m15.dam.local.sportbarradar.homeScreens.view.interfaces;

public interface RegisterBarView {
    void setErrorMail();

    void setErrorEmptyMail();

    void setErrorNotValidMail();

    void setErrorEmptyName();

    void setErrorNotValidPosition();

    void setErrorEmptyPosition();

    void showProgress();

    void hideProgress();

    void cleanErrors();

    void showCancelDialog();

    void setErrorPlayServicesException();

    void redirect();

    void setErrorNotConnection();

    void sendPassword(String mail, String password);
}
