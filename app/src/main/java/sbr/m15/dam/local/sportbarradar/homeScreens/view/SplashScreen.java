package sbr.m15.dam.local.sportbarradar.homeScreens.view;

import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import sbr.m15.dam.local.sportbarradar.R;
import sbr.m15.dam.local.sportbarradar.bases.BaseActivity;

public class SplashScreen extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        ImageView splashImage = (ImageView) findViewById(R.id.splashImage);
        Animation transparencia = AnimationUtils.loadAnimation(this, R.anim.inicio);
        transparencia.reset();
        splashImage.startAnimation(transparencia);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                requestPermissionsAndRedirect();
            }
        }, 3000);
    }
}