package sbr.m15.dam.local.sportbarradar.barScreens;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.ArrayList;

import butterknife.BindView;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.HttpException;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import sbr.m15.dam.local.sportbarradar.R;
import sbr.m15.dam.local.sportbarradar.adapter.LeaguesAdapter;
import sbr.m15.dam.local.sportbarradar.api.ResultadosFutbol;
import sbr.m15.dam.local.sportbarradar.bases.BaseFragment;
import sbr.m15.dam.local.sportbarradar.model.League;
import sbr.m15.dam.local.sportbarradar.model.Ligas;

public class BarListFragment extends BaseFragment implements LeaguesAdapter.LeagueItemClickListener {


    @BindView(R.id.listadoLigas)
    RecyclerView listadoLigas;
    private LeaguesAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<League> ligas;
    private BarStandingFragment b;
    ProgressBar progressBar;
    private Subscription subscription;
    Retrofit retrofit;
    ResultadosFutbol apiService;

    public BarListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        progressBar = (ProgressBar) view.findViewById(R.id.pbHeaderProgress);
        progressBar.setVisibility(View.VISIBLE);
        ligas = new ArrayList<League>();
        mAdapter = new LeaguesAdapter(ligas, this);
        mLayoutManager = new GridLayoutManager(getContext(), 3);
        listadoLigas.setLayoutManager(mLayoutManager);
        listadoLigas.setAdapter(mAdapter);
        apiService = retrofit.create(ResultadosFutbol.class);
        addLeagues("es");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        retrofit = new Retrofit.Builder()
                .baseUrl(ResultadosFutbol.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io()))
                .build();
        return inflater.inflate(R.layout.fragment_list_leagues, container, false);
    }

    private void addLeagues(String es) {
        Observable<Ligas> call = apiService.getLigas(es);
        subscription = call
                .subscribeOn(Schedulers.io()) // optional if you do not wish to override the default behavior
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Subscriber<Ligas>() {
                    @Override
                    public void onCompleted() {
                        mAdapter.notifyDataSetChanged();
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(Throwable e) {
                        // cast to retrofit.HttpException to get the response code
                        if (e instanceof HttpException) {
                            HttpException response = (HttpException) e;
                            e.printStackTrace();
                        }
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(Ligas ta) {

                        for (League l : ta.getCategory().getSpain().getLigas())
                            if (l.getRound() != l.getTotalRounds())
                                ligas.add(l);

                        for (League c : ta.getCategory().getSpain().getCompeticiones())
                            if (c.getRound() != c.getTotalRounds())
                                ligas.add(c);
                    }
                });
    }

    public static BarListFragment newInstance() {
        BarListFragment fragment = new BarListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (null != subscription)
            subscription.unsubscribe();
    }

    @Override
    public void itemClicked(League l) {
        b = BarStandingFragment.newInstance(l);
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.contenedorDePartidos, b, "partidosDeXLiga")
                .commit();
    }
}
