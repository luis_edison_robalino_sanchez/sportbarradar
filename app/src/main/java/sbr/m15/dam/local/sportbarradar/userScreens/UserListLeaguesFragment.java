package sbr.m15.dam.local.sportbarradar.userScreens;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import java.util.ArrayList;

import butterknife.BindView;
import sbr.m15.dam.local.sportbarradar.R;
import sbr.m15.dam.local.sportbarradar.adapter.LeaguesAdapter;
import sbr.m15.dam.local.sportbarradar.bases.BaseFragment;
import sbr.m15.dam.local.sportbarradar.db.Connexion;
import sbr.m15.dam.local.sportbarradar.model.League;

public class UserListLeaguesFragment extends BaseFragment implements LeaguesAdapter.LeagueItemClickListener {

    @BindView(R.id.listadoLigas)
    RecyclerView listadoLigas;
    @BindView(R.id.contenedorDePartidos)
    FrameLayout contenedorDePartidos;
    @BindView(R.id.pbHeaderProgress)
    ProgressBar pbHeaderProgress;

    private RecyclerView.LayoutManager mLayoutManager;
    private LeaguesAdapter mAdapter;
    private ArrayList<League> leagues;

    private UserListMatchesFromLeagueFragment matchesFragment;

    public UserListLeaguesFragment() {
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pbHeaderProgress.setVisibility(View.VISIBLE);
        leagues = new ArrayList<League>();
        Connexion conn = new Connexion();
        try {
            conn.conectar();
            leagues = conn.getLeagues();
        } catch (Exception e) {
            Snackbar.make(getView(), R.string.errorNotConnection, Snackbar.LENGTH_SHORT).show();
            e.printStackTrace();
        } finally {
            if (conn.isConnected()) conn.desconectar();
        }
        mAdapter = new LeaguesAdapter(leagues, this);
        mLayoutManager = new GridLayoutManager(getContext(), 3);
        listadoLigas.setLayoutManager(mLayoutManager);
        listadoLigas.setAdapter(mAdapter);
        pbHeaderProgress.setVisibility(View.GONE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list_leagues, container, false);
    }

    public static UserListLeaguesFragment newInstance() {
        UserListLeaguesFragment fragment = new UserListLeaguesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void itemClicked(League l) {
        matchesFragment = UserListMatchesFromLeagueFragment.newInstance(l);
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.contenedorDePartidos, matchesFragment, "partidosDeXLiga")
                .commit();
    }
}
