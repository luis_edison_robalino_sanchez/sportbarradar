package sbr.m15.dam.local.sportbarradar.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class MatchDay implements Serializable {
    @SerializedName("match")
    @Expose
    private List<Match> matchs = null;

    public List<Match> getMatchs() {
        return matchs;
    }

    public void setStanding(List<Match> matchs) {
        this.matchs = matchs;
    }
}