package sbr.m15.dam.local.sportbarradar.userScreens;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.ArrayList;

import butterknife.BindView;
import sbr.m15.dam.local.sportbarradar.R;
import sbr.m15.dam.local.sportbarradar.adapter.MatchesAdapter;
import sbr.m15.dam.local.sportbarradar.bases.BaseFragment;
import sbr.m15.dam.local.sportbarradar.db.Connexion;
import sbr.m15.dam.local.sportbarradar.model.League;
import sbr.m15.dam.local.sportbarradar.model.Match;

public class UserListMatchesFromLeagueFragment extends BaseFragment implements MatchesAdapter.MatchesItemClickListener {

    @BindView(R.id.listadoLigas)
    RecyclerView listadoLigas;
    @BindView(R.id.pbHeaderProgress)
    ProgressBar pbHeaderProgress;

    private ArrayList<Match> partidos;
    private MatchesAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private UserListBaresFromMatchFragment barFragment;

    public UserListMatchesFromLeagueFragment() {
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pbHeaderProgress.setVisibility(View.VISIBLE);
        partidos = new ArrayList<Match>();
        Connexion conn = new Connexion();
        try {
            conn.conectar();
            partidos = conn.getMatchesFromLeague(getArguments().getString("liga"));
        } catch (Exception e) {
            Snackbar.make(getView(), R.string.errorNotConnection, Snackbar.LENGTH_SHORT).show();
            e.printStackTrace();
        } finally {
            if (conn.isConnected()) conn.desconectar();
        }
        mAdapter = new MatchesAdapter(partidos, this);
        mLayoutManager = new LinearLayoutManager(getContext());
        listadoLigas.setLayoutManager(mLayoutManager);
        listadoLigas.setAdapter(mAdapter);
        pbHeaderProgress.setVisibility(View.GONE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list_leagues, container, false);
    }

    public static UserListMatchesFromLeagueFragment newInstance(League l) {
        UserListMatchesFromLeagueFragment fragment = new UserListMatchesFromLeagueFragment();
        Bundle args = new Bundle();
        args.putString("liga", l.getNombre());
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void itemClicked(Match m) {
        barFragment = UserListBaresFromMatchFragment.newInstance(m);
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.contenedorDeBares, barFragment, "baresDeXPartido")
                .commit();
    }
}
