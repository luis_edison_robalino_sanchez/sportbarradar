package sbr.m15.dam.local.sportbarradar.homeScreens.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import sbr.m15.dam.local.sportbarradar.R;
import sbr.m15.dam.local.sportbarradar.bases.BaseActivity;
import sbr.m15.dam.local.sportbarradar.homeScreens.presenter.RegisterBarPresenterImpl;
import sbr.m15.dam.local.sportbarradar.homeScreens.presenter.interfaces.RegisterBarPresenter;
import sbr.m15.dam.local.sportbarradar.homeScreens.view.interfaces.RegisterBarView;
import sbr.m15.dam.local.sportbarradar.model.Bar;
import sbr.m15.dam.local.sportbarradar.services.MailIntentService;
import sbr.m15.dam.local.sportbarradar.utils.FormUtils;

public class RegisterBarActivity extends BaseActivity implements RegisterBarView {

    private static final int PLACE_PICKER_REQUEST = 1;

    @BindView(R.id.nameBar)
    TextInputLayout nameBar;
    @BindView(R.id.email)
    TextInputLayout email;
    @BindView(R.id.editTextNameBar)
    EditText editTextNameBar;
    @BindView(R.id.editTextEmail)
    EditText editTextEmail;
    @BindView(R.id.wifi)
    CheckBox wifi;
    @BindView(R.id.terrace)
    CheckBox terrace;
    @BindView(R.id.address)
    TextView address;

    private Bar bar;
    private RegisterBarPresenter presenter;
    private PlacePicker.IntentBuilder builder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_bar);
        ButterKnife.bind(this);
        presenter = new RegisterBarPresenterImpl(this);
        bar = new Bar();
        hideProgress();
    }

    @OnClick({R.id.btnCancel, R.id.btnRegister, R.id.place})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnCancel:
                if (!formIsEmpty()) {
                    showCancelDialog();
                } else {
                    finish();
                }
                break;
            case R.id.btnRegister:
                bar.setEmail(editTextEmail.getText().toString());
                bar.setNombre(editTextNameBar.getText().toString());
                bar.setTerraza(terrace.isChecked());
                bar.setWifi(wifi.isChecked());
                presenter.validateRegister(bar);
                break;
            case R.id.place:
                if (builder == null) {
                    builder = new PlacePicker.IntentBuilder();
                    try {
                        startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);
                    } catch (GooglePlayServicesRepairableException e) {
                        setErrorPlayServicesException();
                    } catch (GooglePlayServicesNotAvailableException e) {
                        setErrorPlayServicesException();
                    }
                }
                break;
        }
    }

    private boolean formIsEmpty() {
        if (FormUtils.isNotEmpty(editTextNameBar.getText().toString())) return false;
        if (FormUtils.isNotEmpty(editTextEmail.getText().toString())) return false;
        if (wifi.isChecked()) return false;
        if (terrace.isChecked()) return false;
        return true;
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place p = PlacePicker.getPlace(data, this);
                LatLng coordenadas = p.getLatLng();
                bar.setLat(coordenadas.latitude);
                bar.setLon(coordenadas.longitude);
                address.setText(p.getAddress());
            }
            builder = null;
        }
    }


    @Override
    public void setErrorMail() {
        hideProgress();
        email.setError(getString(R.string.errorEmailExist));
    }

    @Override
    public void setErrorEmptyMail() {
        hideProgress();
        email.setError(getString(R.string.errorEmptyField));
    }

    @Override
    public void setErrorNotValidMail() {
        hideProgress();
        email.setError(getString(R.string.errorEmailNotValid));
    }

    @Override
    public void setErrorEmptyName() {
        hideProgress();
        nameBar.setError(getString(R.string.errorEmptyField));
    }

    @Override
    public void setErrorNotValidPosition() {
        hideProgress();
        Snackbar.make(nameBar, R.string.errorNotValidLocation, Snackbar.LENGTH_LONG)
                .show();
    }

    @Override
    public void setErrorEmptyPosition() {
        hideProgress();
        Snackbar.make(nameBar, R.string.errorEmptyLocation, Snackbar.LENGTH_LONG)
                .show();
    }

    @Override
    public void cleanErrors() {
        hideProgress();
        nameBar.setError("");
        email.setError("");
    }

    @Override
    public void showCancelDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.dialogCancelRegister)
                .setPositiveButton(R.string.btnAccept, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                })
                .setNegativeButton(R.string.btnCancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public void setErrorPlayServicesException() {
        hideProgress();
        Snackbar.make(nameBar, R.string.errorGoogleServices, Snackbar.LENGTH_LONG)
                .show();
    }

    @Override
    public void setErrorNotConnection() {
        hideProgress();
        Snackbar.make(nameBar, R.string.errorNotConnection, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void sendPassword(String mail, String password) {
        MailIntentService.startActionSendMail(this, mail, password);
    }

    @Override
    public void redirect() {
        Intent i = new Intent(this, LoginActivity.class);
        i.putExtra("msgRegister", getString(R.string.ntfCorrectRegisterBar));
        startActivity(i);
        finish();
    }

    @Override
    public void onBackPressed() {
        if (!formIsEmpty()) showCancelDialog();
    }
}