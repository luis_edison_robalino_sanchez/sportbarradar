package sbr.m15.dam.local.sportbarradar.bases;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.View;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import sbr.m15.dam.local.sportbarradar.R;
import sbr.m15.dam.local.sportbarradar.model.Bar;
import sbr.m15.dam.local.sportbarradar.model.SavedLogin;
import sbr.m15.dam.local.sportbarradar.model.User;
import sbr.m15.dam.local.sportbarradar.userScreens.UserMapFragment;
import sbr.m15.dam.local.sportbarradar.utils.FileUtils;

public class BaseFragment extends Fragment {

    protected static final long MIN_TIME = 400;
    protected static final float MIN_DISTANCE = 1000;

    protected final int ICON_ACTIVATE = R.color.colorPrimaryDark;
    protected final int ICON_DESACTIVATE = R.color.colorAccent;

    protected Unbinder unbinder;
    private String filePathSavedLogin;
    private String filePathBar;
    private String filePathUser;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        filePathSavedLogin = getContext().getFilesDir().getPath().toString() + "/savedLogin.dat";
        filePathBar = getContext().getFilesDir().getPath().toString() + "/bar.dat";
        filePathUser = getContext().getFilesDir().getPath().toString() + "/user.dat";
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
    }

    protected void saveFileUserData(User user) {
        FileUtils.saveFileUserData(filePathUser, user);
    }

    protected void saveFileBarData(Bar bar) {
        FileUtils.saveFileBarData(filePathBar, bar);
    }

    protected SavedLogin getSavedLoginFromFile() {
        return FileUtils.getSavedLoginFromFile(filePathSavedLogin);
    }

    protected String getUidFromFile() {
        return FileUtils.getUidFromFile(filePathSavedLogin);
    }

    protected String getEmailFromFile() {
        return FileUtils.getEmailFromFile(filePathSavedLogin);
    }

    protected User getUserFromFile() {
        return FileUtils.getUserFromFile(filePathUser);
    }

    protected Bar getBarFromFile() {
        return FileUtils.getBarFromFile(filePathBar);
    }

    protected void deleteFileSavedLogin() {
        FileUtils.deleteFile(filePathSavedLogin);
    }

    protected void deleteFileUserData() {
        FileUtils.deleteFile(filePathUser);
    }

    protected void deleteFileBarData() {
        FileUtils.deleteFile(filePathBar);
    }

    protected void showDialogConfirmRedirectToMap(final Bar b) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage(R.string.dialogRedirectToMap)
                .setPositiveButton(R.string.btnAccept, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        redirectoToMap(b);
                    }
                })
                .setNegativeButton(R.string.btnCancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void redirectoToMap(Bar b) {
        Bundle bundle = new Bundle();
        bundle.putDouble("latBar", b.getLat());
        bundle.putDouble("lonBar", b.getLon());
        UserMapFragment userMapFragment = new UserMapFragment();
        userMapFragment.setArguments(bundle);
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.contenedorMapa, userMapFragment, "mapa")
                .commit();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}