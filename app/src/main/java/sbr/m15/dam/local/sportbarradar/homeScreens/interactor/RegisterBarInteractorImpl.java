package sbr.m15.dam.local.sportbarradar.homeScreens.interactor;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseNetworkException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.ProviderQueryResult;

import org.apache.commons.lang3.RandomStringUtils;

import sbr.m15.dam.local.sportbarradar.db.Connexion;
import sbr.m15.dam.local.sportbarradar.homeScreens.interactor.interfaces.RegisterBarInteractor;
import sbr.m15.dam.local.sportbarradar.model.Bar;
import sbr.m15.dam.local.sportbarradar.utils.FormUtils;

public class RegisterBarInteractorImpl implements RegisterBarInteractor {
    private FirebaseAuth mAuth;
    private OnRegisterBarFinishedListener listener;
    private String password;
    private Bar bar;

    public RegisterBarInteractorImpl(OnRegisterBarFinishedListener listener) {
        mAuth = FirebaseAuth.getInstance();
        this.listener = listener;
    }

    @Override
    public void validateRegister(Bar bar) {
        boolean isCorrect = true;
        this.bar = bar;

        if (!FormUtils.isNotEmpty(bar.getNombre())) {
            listener.onErrorEmptyName();
            isCorrect = false;
        }

        if (!FormUtils.isEmailValid(bar.getEmail())) {
            listener.onErrorNotValidMail();
            isCorrect = false;
        }

        if (!FormUtils.isNotEmpty(bar.getEmail())) {
            listener.onErrorEmptyMail();
            isCorrect = false;
        }

        if (bar.getLat() == 0) {
            listener.onErrorNotSelectedPosition();
            isCorrect = false;
        }

        // TODO: 06/03/17 Comprobar bar en esa posicion.   

        if (isCorrect) {
            mAuth.fetchProvidersForEmail(bar.getEmail())
                    .addOnCompleteListener(new OnCompleteListener<ProviderQueryResult>() {
                        @Override
                        public void onComplete(@NonNull Task<ProviderQueryResult> task) {
                            if (task.isSuccessful()) {
                                if (task.getResult().getProviders().size() == 0) {
                                    registerBar();
                                } else {
                                    listener.onErrorMail();
                                }
                            } else {
                                try {
                                    throw task.getException();
                                } catch (FirebaseNetworkException e) {
                                    listener.onErrorNotConection();
                                } catch (Exception e) {
                                    listener.onErrorNotConection();
                                }
                            }
                        }
                    });
        }
    }

    private void registerBar() {
        password = RandomStringUtils.randomAlphanumeric(10);
        mAuth.createUserWithEmailAndPassword(bar.getEmail(), password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
                            Connexion conn = new Connexion();
                            try {
                                conn.conectar();
                                if (!conn.isConnected()) throw new Exception();
                                conn.createBar(new Bar(firebaseUser.getUid(), bar.getNombre(),
                                        bar.getLat(), bar.getLon(),
                                        bar.getWifi(), bar.getTerraza()));
                                listener.sendPassword(bar.getEmail(), password);
                                listener.onSuccess();
                            } catch (Exception ex) {
                                FormUtils.deleteUserFirebase(firebaseUser);
                                listener.onErrorNotConection();
                            } finally {
                                if (conn.isConnected()) conn.desconectar();
                            }
                        } else {
                            try {
                                throw task.getException();
                            } catch (FirebaseNetworkException e) {
                                listener.onErrorNotConection();
                            } catch (Exception e) {
                                listener.onErrorNotConection();
                            }
                        }
                    }
                });
    }


}
