package sbr.m15.dam.local.sportbarradar.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import sbr.m15.dam.local.sportbarradar.R;
import sbr.m15.dam.local.sportbarradar.model.Team;

public class TeamAdapter extends ArrayAdapter<Team> {

    public TeamAdapter(@NonNull Context context, @LayoutRes int resource, ArrayList<Team> lista) {
        super(context, resource, lista);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {
        Team t = getItem(position);
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View row = inflater.inflate(R.layout.list_item_team, parent, false);
        TextView label = (TextView) row.findViewById(R.id.nameTeam);
        label.setText(t.getFullName());
        ImageView icon = (ImageView) row.findViewById(R.id.logoTeam);
        if (null != t.getShield())
            Glide.with(getContext()).load(t.getShield()).into(icon);
        return row;
    }
}