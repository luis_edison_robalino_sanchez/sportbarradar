package sbr.m15.dam.local.sportbarradar.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Team implements Serializable {
    @SerializedName("nameShow")
    @Expose
    private String fullName;

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("shield")
    @Expose
    private String shield;

    public Team() {
    }

    public Team(int idEquipo, String nombreEquipo, String fotoEquipo) {
        id = idEquipo;
        fullName = nombreEquipo;
        shield = fotoEquipo;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getShield() {
        return shield;
    }

    public void setShield(String shield) {
        this.shield = shield;
    }

    @Override
    public String toString() {
        return fullName;
    }
}
