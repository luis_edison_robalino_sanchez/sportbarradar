package sbr.m15.dam.local.sportbarradar.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class League implements Serializable{
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("name")
    @Expose
    private String nombre;
    @SerializedName("logo")
    @Expose
    private String logoUrl;
    @SerializedName("current_round")
    @Expose
    private int round;

    @SerializedName("total_rounds")
    @Expose
    private int totalRounds;

    public League() {
    }

    public League(String nombre, String logoUrl) {
        this.nombre = nombre;
        this.logoUrl = logoUrl;
    }

    public League(int id, String nombre, String logoUrl, int round, int totalRounds) {
        this(nombre, logoUrl);
        this.id = id;
        this.round = round;
        this.totalRounds = totalRounds;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public int getRound() {
        return round;
    }

    public void setRound(int round) {
        this.round = round;
    }

    public int getTotalRounds() {
        return totalRounds;
    }

    public void setTotalRounds(int totalRounds) {
        this.totalRounds = totalRounds;
    }

    public int getNextRound() {
        return (++round > totalRounds)?totalRounds:round;
    }
}
