package sbr.m15.dam.local.sportbarradar.homeScreens.presenter;

import sbr.m15.dam.local.sportbarradar.homeScreens.interactor.RegisterUserInteractorImpl;
import sbr.m15.dam.local.sportbarradar.homeScreens.interactor.interfaces.RegisterUserInteractor;
import sbr.m15.dam.local.sportbarradar.homeScreens.presenter.interfaces.RegisterUserPresenter;
import sbr.m15.dam.local.sportbarradar.homeScreens.view.interfaces.RegisterUserView;
import sbr.m15.dam.local.sportbarradar.model.User;

public class RegisterUserPresenterImpl implements RegisterUserPresenter, RegisterUserInteractor.OnRegisterUserFinishedListener {
    private RegisterUserView vista;
    private RegisterUserInteractor interactor;

    public RegisterUserPresenterImpl(RegisterUserView vista) {
        this.vista = vista;
        this.interactor = new RegisterUserInteractorImpl(this);
    }

    @Override
    public void validateRegister(User user) {
        if (vista != null) {
            vista.cleanErrors();
            vista.showProgress();
        }
        interactor.validateRegister(user);
    }

    @Override
    public void onDestroy() {
        vista = null;
    }

    @Override
    public void onErrorEmptyName() {
        vista.setErrorEmptyName();
    }

    @Override
    public void onErrorNotValidMail() {
        vista.setErrorNotValidMail();
    }

    @Override
    public void onErrorEmptyMail() {
        vista.setErrorEmptyMail();
    }

    @Override
    public void onErrorMail() {
        vista.setErrorMail();
    }

    @Override
    public void onErrorEmptyPassword() {
        vista.setErrorEmptyPassword();
    }

    @Override
    public void onErrorEmptyRepeatPassword() {
        vista.setErrorEmptyRepeatPassword();
    }

    @Override
    public void onErrorPasswordNotEqual() {
        vista.setErrorPasswordNotEqual();
    }

    @Override
    public void onErrorShortPassword() {
        vista.setErrorShortPassword();
    }

    @Override
    public void onErrorNotConection() {
        vista.setErrorNotConnection();
    }

    @Override
    public void onSuccess() {
        vista.redirect();
    }

    @Override
    public void onErrorNotSelectedTeam() {
        vista.setErrorTeam();
    }
}
