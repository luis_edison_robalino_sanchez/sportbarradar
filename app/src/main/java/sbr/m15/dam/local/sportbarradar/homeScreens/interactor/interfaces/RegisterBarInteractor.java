package sbr.m15.dam.local.sportbarradar.homeScreens.interactor.interfaces;

import sbr.m15.dam.local.sportbarradar.model.Bar;

public interface RegisterBarInteractor {
    void validateRegister(Bar bar);

    interface OnRegisterBarFinishedListener {

        void onErrorEmptyName();

        void onErrorNotValidMail();

        void onErrorEmptyMail();

        void onErrorMail();

        void onErrorNotConection();

        void onErrorNotSelectedPosition();

        void onErrorNotValidPosition();

        void onSuccess();

        void sendPassword(String mail, String password);
    }
}
