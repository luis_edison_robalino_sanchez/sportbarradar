package sbr.m15.dam.local.sportbarradar.model;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

import java.io.Serializable;

public class Bar implements Serializable, ClusterItem {
    private String uid;
    private String nombre;
    private String email;
    private double lat;
    private double lon;
    private Boolean wifi;
    private Boolean terraza;
    private int distancia;
    private byte[] foto;
    private float valoracion;

    public Bar() {
    }

    public Bar(String nombre) {
        this.nombre = nombre;
    }

    public Bar(String uid, String nombre, double lat, double lon, Boolean wifi, Boolean terraza) {
        this.uid = uid;
        this.nombre = nombre;
        this.lat = lat;
        this.lon = lon;
        this.wifi = wifi;
        this.terraza = terraza;
    }

    public Bar(String uid, String nombre, double lat, double lon, Boolean wifi, Boolean terraza, byte[] foto) {
        this(uid, nombre, lat, lon, wifi, terraza);
        this.foto = foto;
    }

    public Bar(String uid, String nombre, double lat, double lon, Boolean wifi, Boolean terraza, int distancia) {
        this(uid, nombre, lat, lon, wifi, terraza);
        this.distancia = distancia;
    }

    public Bar(String uid, String nombre, double lat, double lon, Boolean wifi, Boolean terraza, byte[] foto, float valoracion, int distancia) {
        this(uid, nombre, lat, lon, wifi, terraza, foto);
        this.valoracion = valoracion;
        this.distancia = distancia;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public Boolean getWifi() {
        return wifi;
    }

    public void setWifi(Boolean wifi) {
        this.wifi = wifi;
    }

    public Boolean getTerraza() {
        return terraza;
    }

    public void setTerraza(Boolean terraza) {
        this.terraza = terraza;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public int getDistancia() {
        return distancia;
    }

    public void setDistancia(int distancia) {
        this.distancia = distancia;
    }

    public byte[] getFoto() {
        return foto;
    }

    public void setFoto(byte[] foto) {
        this.foto = foto;
    }

    public float getValoracion() {
        return valoracion;
    }

    public void setValoracion(float valoracion) {
        this.valoracion = valoracion;
    }

    @Override
    public LatLng getPosition() {
        return new LatLng(lat, lon);
    }

}
