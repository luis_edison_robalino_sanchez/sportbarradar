package sbr.m15.dam.local.sportbarradar.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Match {
    @SerializedName("local")
    @Expose
    private String local;

    @SerializedName("visitor")
    @Expose
    private String visitor;

    @SerializedName("local_shield")
    @Expose
    private String localShield;

    @SerializedName("visitor_shield")
    @Expose
    private String visitorShield;

    @SerializedName("result")
    @Expose
    private String result;

    @SerializedName("date")
    @Expose
    private String date;

    @SerializedName("hour")
    @Expose
    private String hour;

    @SerializedName("minute")
    @Expose
    private String minute;

    @SerializedName("competition_name")
    @Expose
    private String league;

    @SerializedName("id")
    @Expose
    private int api;

    private String uidBar;
    private String leagueShield;
    private String completeDate;

    public Match(String local, String visitor, String localShield, String visitorShield) {
        this.local = local;
        this.visitor = visitor;
        this.localShield = localShield;
        this.visitorShield = visitorShield;
    }

    public Match(int api, String local, String localShield, String visitor, String visitorShield, String league, String leagueShield, String completeDate) {
        this(local, visitor, localShield, visitorShield);
        this.api = api;
        this.league = league;
        this.leagueShield = leagueShield;
        this.completeDate = completeDate;
    }

    public Match(String uidBar, int api, String local, String localShield, String visitor, String visitorShield, String league, String leagueShield, String completeDate) {
        this(api, local, localShield, visitor, visitorShield, league, leagueShield, completeDate);
        this.uidBar = uidBar;
    }

    public String getUidBar() {
        return uidBar;
    }

    public void setUidBar(String uidBar) {
        this.uidBar = uidBar;
    }

    public int getApi() {
        return api;
    }

    public void setApi(int api) {
        this.api = api;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public String getVisitor() {
        return visitor;
    }

    public void setVisitor(String visitor) {
        this.visitor = visitor;
    }

    public String getLocalShield() {
        return localShield;
    }

    public void setLocalShield(String localShield) {
        this.localShield = localShield;
    }

    public String getVisitorShield() {
        return visitorShield;
    }

    public void setVisitorShield(String visitorShield) {
        this.visitorShield = visitorShield;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getMinute() {
        return minute;
    }

    public void setMinute(String minute) {
        this.minute = minute;
    }

    public String getLeague() {
        return league;
    }

    public void setLeague(String league) {
        this.league = league;
    }

    public String getLeagueShield() {
        return leagueShield;
    }

    public void setLeagueShield(String leagueShield) {
        this.leagueShield = leagueShield;
    }

    public String getCompleteDate() {
        if (completeDate != null) {
            return completeDate;
        } else {
            return date + " " + hour + ":" + minute;
        }
    }

    public void setCompleteDate(String completeDate) {
        this.completeDate = completeDate;
    }

    public Timestamp getTimestamp() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        if (completeDate != null) {
            try {
                Date parsedDate = dateFormat.parse(completeDate);
                return new java.sql.Timestamp(parsedDate.getTime());
            } catch (Exception e) {
                return null;
            }
        } else {
            try {
                Date parsedDate = dateFormat.parse(date + " " + hour + ":" + minute);
                return new java.sql.Timestamp(parsedDate.getTime());
            } catch (Exception e) {
                return null;
            }
        }
    }
}
