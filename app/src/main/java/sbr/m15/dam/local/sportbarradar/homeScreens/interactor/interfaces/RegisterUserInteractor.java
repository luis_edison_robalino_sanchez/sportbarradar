package sbr.m15.dam.local.sportbarradar.homeScreens.interactor.interfaces;

import sbr.m15.dam.local.sportbarradar.model.User;

public interface RegisterUserInteractor {
    void validateRegister(User user);

    interface OnRegisterUserFinishedListener {

        void onErrorEmptyName();

        void onErrorNotValidMail();

        void onErrorEmptyMail();

        void onErrorMail();

        void onErrorEmptyPassword();

        void onErrorEmptyRepeatPassword();

        void onErrorPasswordNotEqual();

        void onErrorShortPassword();

        void onErrorNotConection();

        void onSuccess();

        void onErrorNotSelectedTeam();
    }
}
