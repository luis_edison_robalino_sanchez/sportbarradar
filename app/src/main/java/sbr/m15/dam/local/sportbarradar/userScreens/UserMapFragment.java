package sbr.m15.dam.local.sportbarradar.userScreens;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterManager;

import java.util.ArrayList;

import butterknife.BindView;
import sbr.m15.dam.local.sportbarradar.R;
import sbr.m15.dam.local.sportbarradar.adapter.PickersAdapter;
import sbr.m15.dam.local.sportbarradar.bases.BaseFragment;
import sbr.m15.dam.local.sportbarradar.db.Connexion;
import sbr.m15.dam.local.sportbarradar.model.Bar;

public class UserMapFragment extends BaseFragment implements LocationListener, ClusterManager.OnClusterItemInfoWindowClickListener<Bar> {

    @BindView(R.id.mapview)
    MapView mapview;

    private ClusterManager<Bar> mClusterManager;
    private LocationManager locationManager;
    private GoogleMap googleMap;
    CameraPosition mPreviousCameraPosition = null;

    public UserMapFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static UserMapFragment newInstance() {
        UserMapFragment fragment = new UserMapFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mapview.onCreate(savedInstanceState);
        mapview.onResume();
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mapview.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;
                setUpMap();
            }
        });
    }

    private void setUpMap() {
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setCompassEnabled(true);
        showLocationPermission();
        mClusterManager = new ClusterManager<Bar>(getContext(), googleMap);

        googleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                CameraPosition position = googleMap.getCameraPosition();
                if (mPreviousCameraPosition == null || mPreviousCameraPosition.zoom != position.zoom) {
                    mPreviousCameraPosition = googleMap.getCameraPosition();
                    mClusterManager.cluster();
                }
            }
        });

        googleMap.setOnInfoWindowClickListener(mClusterManager);
        googleMap.setOnMarkerClickListener(mClusterManager);
        for (Bar bar : getAllBar()) mClusterManager.addItem(bar);

        mClusterManager.setOnClusterItemInfoWindowClickListener(this);
        mClusterManager.setOnClusterItemClickListener(
                new ClusterManager.OnClusterItemClickListener<Bar>() {
                    @Override
                    public boolean onClusterItemClick(Bar bar) {
                        googleMap.setInfoWindowAdapter(new PickersAdapter(bar, getContext()));
                        return false;
                    }
                }
        );

        if (!getArguments().isEmpty()) {
            LatLng latLng = new LatLng(getArguments().getDouble("latBar"),
                    getArguments().getDouble("lonBar"));
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 18);
            googleMap.animateCamera(cameraUpdate);
            locationManager.removeUpdates(this);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_user_map, container, false);
    }

    public ArrayList<Bar> getAllBar() {
        ArrayList<Bar> bares = null;
        Connexion conn = new Connexion();
        try {
            conn.conectar();
            bares = conn.getAllBar();
        } catch (Exception e) {
            Snackbar.make(getView(), R.string.errorNotConnection, Snackbar.LENGTH_SHORT).show();
            e.printStackTrace();
        } finally {
            if (conn.isConnected()) conn.desconectar();
        }
        return bares;
    }

    @Override
    public void onLocationChanged(Location location) {
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 18);
        googleMap.animateCamera(cameraUpdate);
    }

    private void showLocationPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME, MIN_DISTANCE, this); //You can also use LocationManager.GPS_PROVIDER and LocationManager.PASSIVE_PROVIDER
            googleMap.setMyLocationEnabled(true);
        } else {
            Snackbar.make(mapview, R.string.msgLocaltionPermission, Snackbar.LENGTH_INDEFINITE)
                    .setAction(getString(R.string.btnActivate), new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
                        }
                    })
                    .show();
        }
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
    }

    @Override
    public void onProviderEnabled(String s) {
    }

    @Override
    public void onProviderDisabled(String s) {
    }

    @Override
    public void onClusterItemInfoWindowClick(Bar bar) {
        Intent intent = new Intent(getContext(), UserViewBarActivity.class);
        intent.putExtra("uidBar", bar.getUid());
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (null != mapview)
            mapview.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (null != mapview)
            mapview.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if (null != mapview)
            mapview.onLowMemory();
    }
}
