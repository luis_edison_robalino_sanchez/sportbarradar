package sbr.m15.dam.local.sportbarradar.dialogs;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Spinner;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.HttpException;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import sbr.m15.dam.local.sportbarradar.R;
import sbr.m15.dam.local.sportbarradar.adapter.TeamAdapter;
import sbr.m15.dam.local.sportbarradar.api.ResultadosFutbol;
import sbr.m15.dam.local.sportbarradar.bases.BaseDialogFragment;
import sbr.m15.dam.local.sportbarradar.db.Connexion;
import sbr.m15.dam.local.sportbarradar.model.Team;
import sbr.m15.dam.local.sportbarradar.model.Teams;
import sbr.m15.dam.local.sportbarradar.utils.FileUtils;


public class ChangeFavTeamDialog extends BaseDialogFragment {

    private String filePathUser;

    @BindView(R.id.changeTeam_list)
    Spinner lista;

    private String uidUser;

    private TeamAdapter adapter;
    private ArrayList<Team> teams;
    private int positionSelected;

    private View rootView;
    private Subscription subscription;
    Retrofit retrofit;
    ResultadosFutbol apiService;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rootView = getActivity().getWindow().getDecorView().getRootView();
        filePathUser = getContext().getFilesDir().getPath().toString() + "/user.dat";
        uidUser = getArguments().getString("uidUser");
        apiService = retrofit.create(ResultadosFutbol.class);
        teams = new ArrayList<>();
        showTeams();
    }

    private void showTeams() {
        Observable<Teams> call = apiService.getTeams();
        subscription = call
                .subscribeOn(Schedulers.io()) // optional if you do not wish to override the default behavior
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Subscriber<Teams>() {
                    @Override
                    public void onCompleted() {
                        adapter = new TeamAdapter(getContext(), R.layout.list_item_team, teams);
                        lista.setAdapter(adapter);
                        lista.setSelection(positionSelected);
                    }

                    @Override
                    public void onError(Throwable e) {
                        // cast to retrofit.HttpException to get the response code
                        if (e instanceof HttpException) {
                            HttpException response = (HttpException) e;
                            e.printStackTrace();
                        }
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(Teams t) {
                        teams.addAll(t.getEquipos());
                        positionSelected = getPositionSelected(teams);
                    }
                });
    }

    private int getPositionSelected(ArrayList<Team> teams) {
        String team = FileUtils.getFavTeam(filePathUser).getFullName();
        for (int i = 0; i < teams.size(); i++)
            if (teams.get(i).getFullName().equals(team)) return i;
        return -1;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        retrofit = new Retrofit.Builder()
                .baseUrl(ResultadosFutbol.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io()))
                .build();
        return inflater.inflate(R.layout.dialog_change_team, container, false);
    }

    @OnClick({R.id.cancelChangeTeam, R.id.sendChangeTeam})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cancelChangeTeam:
                dismiss();
                break;
            case R.id.sendChangeTeam:
                Team team = (Team) lista.getSelectedItem();
                Connexion conn = new Connexion();
                try {
                    conn.conectar();
                    conn.updateFavTeam(uidUser, team);
                    Snackbar.make(rootView, R.string.msgUpdateFavTeam, Snackbar.LENGTH_SHORT).show();
                    FileUtils.changeFavTeam(filePathUser, team);
                    Glide.with(getContext()).load(team.getShield()).into((ImageView) rootView.findViewById(R.id.wifi));
                } catch (Exception e) {
                    Snackbar.make(rootView, R.string.errorNotConnection, Snackbar.LENGTH_SHORT).show();
                    e.printStackTrace();
                } finally {
                    if (conn.isConnected()) conn.desconectar();
                }
                dismiss();
                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (null != subscription)
            subscription.unsubscribe();
    }
}
