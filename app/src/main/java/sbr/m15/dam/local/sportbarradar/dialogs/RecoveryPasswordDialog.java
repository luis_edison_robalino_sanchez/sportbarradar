package sbr.m15.dam.local.sportbarradar.dialogs;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.Unbinder;
import sbr.m15.dam.local.sportbarradar.R;
import sbr.m15.dam.local.sportbarradar.bases.BaseDialogFragment;
import sbr.m15.dam.local.sportbarradar.utils.FormUtils;

public class RecoveryPasswordDialog extends BaseDialogFragment {

    @BindView(R.id.dialogEmail)
    TextInputLayout dialogEmail;
    @BindView(R.id.dialogEditTextEmail)
    TextInputEditText dialogEditTextEmail;

    RecoveryDialogListener mListener;
    protected Unbinder unbinder;

    public interface RecoveryDialogListener {
        void onRecoveryPositiveClick(DialogFragment dialog, String mail);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_recovery_password, container, false);
    }

    @OnClick({R.id.cancelRecovery, R.id.sendRecovery})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cancelRecovery:
                dismiss();
                break;
            case R.id.sendRecovery:
                String mail = dialogEditTextEmail.getText().toString();
                if (FormUtils.isEmailValid(mail)) {
                    mListener.onRecoveryPositiveClick(this, mail);
                } else {
                    dialogEmail.setError(getString(R.string.errorEmailNotValid));
                }
                break;
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (RecoveryDialogListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }
}
