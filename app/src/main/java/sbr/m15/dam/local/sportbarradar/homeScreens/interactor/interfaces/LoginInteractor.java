package sbr.m15.dam.local.sportbarradar.homeScreens.interactor.interfaces;

import sbr.m15.dam.local.sportbarradar.model.Bar;
import sbr.m15.dam.local.sportbarradar.model.User;

public interface LoginInteractor {
    void requestResetPassword(String email);

    void login(String username, String password);

    interface OnLoginFinishedListener {
        void onMailError();

        void onMailNotValid();

        void onMailEmpty();

        void onPasswordEmpty();

        void onPasswordError();

        void onSuccess(Class home);

        void onNotConnection();

        void onRecoverySuccessful();

        void onRecoveryMailError();

        void onRecoveryNotConnection();

        void saveSavedData(String uid, String role, String email);

        void saveUserData(User user);

        void saveBarData(Bar bar);
    }
}
