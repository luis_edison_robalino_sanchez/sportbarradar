package sbr.m15.dam.local.sportbarradar.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import sbr.m15.dam.local.sportbarradar.R;
import sbr.m15.dam.local.sportbarradar.model.Match;
import sbr.m15.dam.local.sportbarradar.viewHolder.MatchesLeagueViewHolder;

public class MatchesLeagueAdapter extends RecyclerView.Adapter<MatchesLeagueViewHolder> {

    ArrayList<Match> matches;
    MatchesLeagueItemClickListener listener;

    public MatchesLeagueAdapter(ArrayList<Match> matches) {
        this.matches = matches;
    }

    public MatchesLeagueAdapter(ArrayList<Match> matches, MatchesLeagueItemClickListener l) {
        this.matches = matches;
        listener = l;
    }

    public static interface MatchesLeagueItemClickListener {
        void itemClicked(Match m);
    }

    @Override
    public MatchesLeagueViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_matches_bar, parent, false);
        MatchesLeagueViewHolder vvh = new MatchesLeagueViewHolder(itemView);
        return vvh;
    }

    @Override
    public void onBindViewHolder(MatchesLeagueViewHolder holder, final int position) {
        holder.bindMatches(matches.get(position));
        if (listener != null)
            holder.cardMatch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.itemClicked(matches.get(position));
                }
            });
    }

    @Override
    public int getItemCount() {
        return matches.size();
    }

}
