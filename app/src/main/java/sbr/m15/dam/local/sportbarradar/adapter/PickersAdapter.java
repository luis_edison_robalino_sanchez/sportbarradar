package sbr.m15.dam.local.sportbarradar.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import sbr.m15.dam.local.sportbarradar.R;
import sbr.m15.dam.local.sportbarradar.model.Bar;

public class PickersAdapter implements GoogleMap.InfoWindowAdapter {

    protected final int ICON_ACTIVATE = R.color.colorPrimaryDark;
    protected final int ICON_DESACTIVATE = R.color.colorAccent;

    private TextView nameBar;
    private ImageView wifi;
    private ImageView terrace;
    private RatingBar ratingValorations;

    private Context context;
    private Bar bar;
    private final View view;

    public PickersAdapter(Bar bar, Context context) {
        this.bar = bar;
        this.context = context;
        view = View.inflate(context, R.layout.picker_info_window, null);
        nameBar = (TextView) view.findViewById(R.id.nameBar);
        wifi = (ImageView) view.findViewById(R.id.wifi);
        terrace = (ImageView) view.findViewById(R.id.terrace);
        ratingValorations = (RatingBar) view.findViewById(R.id.ratingValorations);
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        nameBar.setText(bar.getNombre());
        wifi.setColorFilter((bar.getWifi()) ?
                ContextCompat.getColor(context, ICON_ACTIVATE) :
                ContextCompat.getColor(context, ICON_DESACTIVATE));
        terrace.setColorFilter((bar.getTerraza()) ?
                ContextCompat.getColor(context, ICON_ACTIVATE) :
                ContextCompat.getColor(context, ICON_DESACTIVATE));
        if (bar.getValoracion() == -1) ratingValorations.setVisibility(View.GONE);
        else ratingValorations.setRating(bar.getValoracion());
        return view;
    }
}
