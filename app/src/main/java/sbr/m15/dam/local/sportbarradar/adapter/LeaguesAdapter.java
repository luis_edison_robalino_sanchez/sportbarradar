package sbr.m15.dam.local.sportbarradar.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import sbr.m15.dam.local.sportbarradar.R;
import sbr.m15.dam.local.sportbarradar.model.League;
import sbr.m15.dam.local.sportbarradar.viewHolder.LeaguesViewHolder;

public class LeaguesAdapter extends RecyclerView.Adapter<LeaguesViewHolder> {

    ArrayList<League> ligas;
    LeagueItemClickListener listener;

    public static interface LeagueItemClickListener {
        void itemClicked(League l);

    }

    public LeaguesAdapter(ArrayList<League> ligas, LeagueItemClickListener l) {
        this.ligas = ligas;
        listener = l;
    }

    @Override
    public LeaguesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_leagues, parent, false);
        LeaguesViewHolder lvh = new LeaguesViewHolder(itemView);
        return lvh;
    }

    @Override
    public void onBindViewHolder(LeaguesViewHolder holder, final int position) {
        holder.bindLeague(ligas.get(position));
        holder.cardLeague.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.itemClicked(ligas.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return ligas.size();
    }
}
