package sbr.m15.dam.local.sportbarradar.dialogs;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.OnClick;
import sbr.m15.dam.local.sportbarradar.R;
import sbr.m15.dam.local.sportbarradar.bases.BaseDialogFragment;

public class RegisterOptionsDialog extends BaseDialogFragment {
    public interface RegistrationDialogListener {
        void onUserRoleSelected();

        void onBarRoleSelected();
    }

    protected RegistrationDialogListener listener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_register_select_type, container, false);
    }

    @OnClick({R.id.userRole, R.id.barRole, R.id.cancel})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.userRole:
                listener.onUserRoleSelected();
                break;
            case R.id.barRole:
                listener.onBarRoleSelected();
                break;
        }
        dismiss();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (RegistrationDialogListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }
}
