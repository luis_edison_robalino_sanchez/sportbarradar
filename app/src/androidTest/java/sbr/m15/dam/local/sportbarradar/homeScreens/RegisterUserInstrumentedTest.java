package sbr.m15.dam.local.sportbarradar.homeScreens;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import sbr.m15.dam.local.sportbarradar.R;
import sbr.m15.dam.local.sportbarradar.db.Connexion;
import sbr.m15.dam.local.sportbarradar.homeScreens.view.RegisterUserActivity;
import sbr.m15.dam.local.sportbarradar.utils.TestUtils;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class RegisterUserInstrumentedTest {

    @Rule
    public ActivityTestRule<RegisterUserActivity> mActivityRule =
            new ActivityTestRule<>(RegisterUserActivity.class);

    @Test
    public void testSetFullNameNull() throws Exception {
        // Set values
        onView(withId(R.id.editTextFullName)).perform(typeText(""), closeSoftKeyboard());

        // Action
        onView(withId(R.id.btnRegister)).perform(click());

        // Expected result
        onView(withId(R.id.fullName)).check(matches(TestUtils.hasTextInputLayoutErrorText(
                mActivityRule.getActivity().getString(R.string.errorEmptyField))));
    }

    @Test
    public void testSetFullNameValidValue() throws Exception {
        onView(withId(R.id.editTextFullName)).perform(typeText("validValue"),
                closeSoftKeyboard());

        onView(withId(R.id.btnRegister)).perform(click());

        onView(withId(R.id.fullName)).check(matches(TestUtils.hasTextInputLayoutErrorText("")));
    }

    @Test
    public void testSetEmailNull() throws Exception {
        onView(withId(R.id.editTextEmail)).perform(typeText(""), closeSoftKeyboard());

        onView(withId(R.id.btnRegister)).perform(click());

        onView(withId(R.id.email)).check(matches(
                TestUtils.hasTextInputLayoutErrorText(
                        mActivityRule.getActivity().getString(R.string.errorEmptyField))));
    }

    @Test
    public void testSetEmailNotValidValue() throws Exception {
        onView(withId(R.id.editTextEmail)).perform(typeText("notValidValue"), closeSoftKeyboard());

        onView(withId(R.id.btnRegister)).perform(click());

        onView(withId(R.id.email)).check(matches(
                TestUtils.hasTextInputLayoutErrorText(
                        mActivityRule.getActivity().getString(R.string.errorEmailNotValid))));
    }

    @Test
    public void testSetEmailValidValue() throws Exception {
        onView(withId(R.id.editTextEmail)).perform(typeText("validValue@gmail.com"),
                closeSoftKeyboard());

        onView(withId(R.id.btnRegister)).perform(click());

        onView(withId(R.id.email)).check(matches(TestUtils.hasTextInputLayoutErrorText("")));
    }

    @Test
    public void testSetPasswordNull() throws Exception {
        onView(withId(R.id.editTextPassword)).perform(typeText(""), closeSoftKeyboard());

        onView(withId(R.id.btnRegister)).perform(click());

        onView(withId(R.id.password)).check(matches(TestUtils.hasTextInputLayoutErrorText(
                mActivityRule.getActivity().getString(R.string.errorEmptyField))));
    }

    @Test
    public void testSetPasswordShortValue() throws Exception {
        onView(withId(R.id.editTextPassword)).perform(typeText("1234"), closeSoftKeyboard());

        onView(withId(R.id.btnRegister)).perform(click());

        onView(withId(R.id.password)).check(matches(TestUtils.hasTextInputLayoutErrorText(
                mActivityRule.getActivity().getString(R.string.errorShortPassword))));
    }

    @Test
    public void testSetRepeatPasswordNull() throws Exception {
        onView(withId(R.id.editTextRepeatPassword)).perform(typeText(""), closeSoftKeyboard());

        onView(withId(R.id.btnRegister)).perform(click());

        onView(withId(R.id.repeatPassword)).check(matches(TestUtils.hasTextInputLayoutErrorText(
                mActivityRule.getActivity().getString(R.string.errorEmptyField))));
    }

    @Test
    public void testSetPasswordsDifferentsValues() throws Exception {
        onView(withId(R.id.editTextPassword)).perform(typeText("validPassword"),
                closeSoftKeyboard());
        onView(withId(R.id.editTextRepeatPassword)).perform(typeText("differentPassword"),
                closeSoftKeyboard());

        onView(withId(R.id.btnRegister)).perform(click());

        onView(withId(R.id.password)).check(matches(TestUtils.hasTextInputLayoutErrorText(
                mActivityRule.getActivity().getString(R.string.errorPasswordNotEqual))));
        onView(withId(R.id.repeatPassword)).check(matches(
                TestUtils.hasTextInputLayoutErrorText("")));
    }

    @Test
    public void testSetPasswordsValidValues() throws Exception {
        onView(withId(R.id.editTextPassword)).perform(typeText("validPassword"),
                closeSoftKeyboard());
        onView(withId(R.id.editTextRepeatPassword)).perform(typeText("validPassword"),
                closeSoftKeyboard());

        onView(withId(R.id.btnRegister)).perform(click());

        onView(withId(R.id.password)).check(matches(TestUtils.hasTextInputLayoutErrorText("")));
        onView(withId(R.id.repeatPassword)).check(matches(
                TestUtils.hasTextInputLayoutErrorText("")));
    }

    @Test
    public void testCreateUser() throws Exception {
        onView(withId(R.id.editTextFullName)).perform(typeText("Prueba951753"), closeSoftKeyboard());
        onView(withId(R.id.editTextEmail)).perform(typeText("prueba951753@gmail.com"),
                closeSoftKeyboard());
        onView(withId(R.id.editTextPassword)).perform(typeText("plijygr"), closeSoftKeyboard());
        onView(withId(R.id.editTextRepeatPassword)).perform(typeText("plijygr"), closeSoftKeyboard());

        onView(withId(R.id.btnRegister)).perform(click());

        Connexion conn = new Connexion();
        conn.conectar();
        Thread.sleep(6000);

        if(conn.existUserFromFullName("Prueba951753")) {
            conn.deleteUsuarioFromFullName("Prueba951753");
            TestUtils.deleteUserFirebaseWithEmailAndPassword("prueba951753@gmail.com", "plijy");
            conn.desconectar();
            assertEquals(true, true);
        }else{
            conn.desconectar();
            assertEquals(true, false);
        }
    }
}
