package sbr.m15.dam.local.sportbarradar.utils;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.view.View;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.internal.matchers.TypeSafeMatcher;

public class TestUtils {

    public static Matcher<View> hasTextInputLayoutErrorText(final String expectedErrorText) {
        return new TypeSafeMatcher<View>() {
            @Override
            public boolean matchesSafely(View view) {
                if (!(view instanceof TextInputLayout)) { return false; }
                CharSequence error = ((TextInputLayout) view).getError();
                if (error == null) { return false; }
                String errorString = error.toString();
                return expectedErrorText.equals(errorString);
            }

            @Override
            public void describeTo(Description description) {  }
        };
    }

    public static void deleteUserFirebaseWithEmailAndPassword(String email, String password){
        FirebaseAuth mAuth = mAuth = FirebaseAuth.getInstance();
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Intent intent;
                            FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
                            FormUtils.deleteUserFirebase(firebaseUser);
                        }
                    }
                });
    }
}
